package org.apache.cordova.media;

import android.content.Context;
import android.media.AudioManager;
import android.media.SoundPool;

public class SoundManager 
{
	private Context pContext;
	private SoundPool sndPool;
	private float rate = 1.0f;
	private float masterVolume = 1.0f;
	private static float leftVolume = 0.5f;
	private static float rightVolume = 0.5f;
	
	private static float leftV = 0.5f;
	private static float rightV= 0.5f;
	
	
	
    public static float getLeftV() {
		return leftV;
	}

	public static void setLeftV(float leftV) {
		SoundManager.leftV = leftV;
	}

	public static float getRightV() {
		return rightV;
	}

	public static void setRightV(float rightV) {
		SoundManager.rightV = rightV;
	}




	private float balance = 0.0f;
    private float balance1 = 0.0f;
	// Constructor, setup the audio manager and store the app context
	public SoundManager(Context appContext)
	{
	  sndPool = new SoundPool(16, AudioManager.STREAM_MUSIC, 100);
 	  pContext = appContext;
	}
	
	// Load up a sound and return the id
	public int load(int sound_id)
	{
		return sndPool.load(pContext, sound_id, 1);
	}
	
	// Play a sound
	public void play(int sound_id)
	{
		sndPool.play(sound_id, leftVolume, rightVolume, 1, 0, rate); 	
		
	}	
	
	// Set volume values based on existing balance value
	public void setVolume(float vol)
	{
		masterVolume = vol;
		
		if(balance < 1.0f)
		{
			leftVolume = masterVolume;
			rightVolume = masterVolume * balance;
		}
		else
		{
			rightVolume = masterVolume;
			leftVolume = masterVolume * ( 2.0f - balance );
		}

	}
/*	
	public void setSpeed(float speed)
	{
		rate = speed;
		
		// Speed of zero is invalid 
		if(rate < 0.01f)
			rate = 0.01f;
		
		// Speed has a maximum of 2.0
		if(rate > 2.0f)
			rate = 2.0f;
	}*/
	
	public static float setBalance(float balVal)
	{

		leftVolume=balVal;		

		return leftVolume;
	}

	public static float setBalance1(float balVal1)
	{
		
		rightVolume=balVal1;
		

		return rightVolume;
	}
	
	
	public static float getBalance1(){
		return rightVolume;
	}

	
	public static float getBalance(){
		return leftVolume;
	}

	
	// Free ALL the things!
	public void unloadAll()
	{
		sndPool.release();		
	}

}
