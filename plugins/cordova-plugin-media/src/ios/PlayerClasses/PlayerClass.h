//
//  PlayerClass.h
//  AuraApp
//
//  Created by Serge Gorbachev on 4/26/11.
//  Copyright 2011 Rosberry. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "oalQueuePlayback.h"
#import <UIKit/UIKit.h>

@class Navigator;
@protocol A440Player;
@interface PlayerClass : NSObject<AVAudioSessionDelegate> {
    id<A440Player> _player;
    NSMutableArray *listOfHerz;
    int count;
    int stop;
    int cycl;
    int curentHerz;
    float hz;
    Navigator *__unsafe_unretained nav;
    UIButton *__unsafe_unretained sender;
    int curentTimeF;
    NSArray *arrayF;
//    AVAudioPlayer *audioPlayer;
    
    NSMutableArray *powAr;
    NSMutableData *full;
}

@property(nonatomic,strong) NSMutableArray *powAr;
@property(nonatomic,strong) NSMutableData *full;
@property(nonatomic,strong) NSMutableData *recording;

@property(nonatomic,readonly)int count;
@property(nonatomic,readonly)int stop;

@property(nonatomic,assign)float rightVolume;
@property(nonatomic,assign)float leftVolume;

@property (nonatomic,strong) AVAudioPlayer *audioPlayerLeft;
@property (nonatomic,strong) AVQueuePlayer *queuePlayer;
@property(nonatomic,strong) NSArray *arrayF;
@property(nonatomic,unsafe_unretained) Navigator *nav;
@property(nonatomic,unsafe_unretained) UIButton *sender;
@property(nonatomic,strong) NSMutableArray *listOfHerz;

@property( nonatomic, strong ) NSTimer	*tmStopPlaying;
@property( nonatomic, strong ) NSTimer	*tm2;

//! Set this property to Date, when player begins to play.
@property( nonatomic, strong ) NSDate	*referenceBeginPlay;

-(void)playOfTrack:(NSArray*)arOfTrack timer:(int)min sendButton:(UIButton*) but;
-(void)playListOfSound:(NSArray*)arOfHerz timer:(int)min sendButton:(UIButton*) but;
-(void)playOfSoundFibonachi:(float)arOfHerz timer:(int)min sendButton:(UIButton*) but;
- (void)stopPlayer;
- (void)playPlayer;
-(void)playList;
-(void)stopMyTimer;
-(void)scanOfSound;
-(void)soundOf528PlayLong;
-(void)stopOf528Play;
-(void)playSoundOf528;
-(void)SoundOf528; 
-(void)newBuffer;
-(void)scanOfSoundWithLove;

-(void)killPlayer;

+ (void) setAudioGenerationAlgorithmBowl:(BOOL)isBowl;
+ (void) reinitAudioGenerationAlgorithm;

@end
