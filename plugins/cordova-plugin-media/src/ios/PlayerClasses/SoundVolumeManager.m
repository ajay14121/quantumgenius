//
//  SoundVolumeManager.m
//  AuraApp
//
//  Created by Vitaliy T on 05/11/14.
//  Copyright (c) 2014 Quantum Life LLC. All rights reserved.
//

#import "SoundVolumeManager.h"

//NSString *kSoundVolumeManagerNotification_scanVolumeChanged = @"scanVolumeChanged";

static NSString		*kScanVolumeSlider_key = @"scanVolumeSlider";
static float		kScanVolumeSlider_defaultValue = 1.0f;
static const float	kMinGainDecibeles = -40.0f;
static const float	kMaxGainDecibeles = 0.0f;

@interface SoundVolumeManager (/* Private */)

//! Common instance variable initializer
- (void) commonInit;

//! Converts [0.0 1.0] scale from settings to [-40dB +0dB] amplification
- (float) logScaleGainFromSettingsVolumeSliderValue:(float)sliderValue;

//! Scales normalized sliderValue [0.0; 1.0] to [minValue; maxValue] linear
- (float) scaleSliderValue:(float)sliderValue withMinGaudge:(float)minValue withMaxGaudge:(float)maxValue;

@end // @interface SoundVolumeManager


@implementation SoundVolumeManager

@synthesize scanVolumeSliderNormalizedValue = _scanVolumeSliderNormalizedValue;

+ (instancetype) sharedInstance
{
	static id s_instance;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^
	{
		s_instance = [[self alloc] init];
	});
	
	return s_instance;
}

- (instancetype) init
{
	self = [super init];
	if( self )
	{
		[self commonInit];
	}
	
	return self;
}

- (void) commonInit
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id savedScanVolume = [defaults valueForKey:kScanVolumeSlider_key];

	if( !savedScanVolume )
	{
		_scanVolumeSliderNormalizedValue = kScanVolumeSlider_defaultValue;
		[defaults setValue:@(kScanVolumeSlider_defaultValue)
					forKey:kScanVolumeSlider_key];
		[defaults synchronize];
	}
	else
	{
		_scanVolumeSliderNormalizedValue = [defaults floatForKey:kScanVolumeSlider_key];
	}
}


#pragma mark - API methods

- (float) scanGain
{
	return ([self shouldMuteWithSliderValue:_scanVolumeSliderNormalizedValue])
		?0
		:[self logScaleGainFromSettingsVolumeSliderValue:_scanVolumeSliderNormalizedValue];
}

- (NSString *) stringRepresentationForLogScale
{
	if( [self shouldMuteWithSliderValue:_scanVolumeSliderNormalizedValue] )
	{
		return NSLocalizedString( @"Muted", @"Scan Volume = no sound" );
	}
		
	float gain_dB = [self scaleSliderValue:_scanVolumeSliderNormalizedValue
							 withMinGaudge:kMinGainDecibeles
							 withMaxGaudge:kMaxGainDecibeles];
	int gainInt = roundf(gain_dB);
	return [NSString stringWithFormat:@"%ddB", gainInt];
}


#pragma mark - Log scale math helper

//! Converts [0.0 1.0] scale from settings to [-40dB +0dB] amplification
- (float) logScaleGainFromSettingsVolumeSliderValue:(float)sliderValue
{
	float dBGain = [self scaleSliderValue:sliderValue
							withMinGaudge:kMinGainDecibeles
							withMaxGaudge:kMaxGainDecibeles];
	return powf( 10.0f, dBGain/20 ); // each 20dB for 10x amplification
}

- (float) scaleSliderValue:(float)sliderValue withMinGaudge:(float)minValue withMaxGaudge:(float)maxValue;
{
	NSParameterAssert( minValue < maxValue );
	float scaleRange = maxValue - minValue;
	
	if( sliderValue < 1e-9 )
	{// Too small value, return pure -∞ gain
		return minValue;
	}
	
	if( sliderValue >= 1.0f )
	{// High value, return pure +0dB gain
		return maxValue;
	}
	
	return minValue + sliderValue*scaleRange;
}

- (BOOL) shouldMuteWithSliderValue:(float)sliderValue
{
	return ( sliderValue < 1e-9 );
}


#pragma mark - Custom property methods

- (float) scanVolumeSliderNormalizedValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	id savedScanVolume = [defaults valueForKey:kScanVolumeSlider_key];
	NSParameterAssert( savedScanVolume );

	return (float)[savedScanVolume floatValue];
}

- (void) setScanVolumeSliderNormalizedValue:(float)scanVolumeSliderNormalizedValue
{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

	[self willChangeValueForKey:@"scanVolumeSliderNormalizedValue"];
	_scanVolumeSliderNormalizedValue = scanVolumeSliderNormalizedValue;
	[defaults setValue:@(scanVolumeSliderNormalizedValue)
				forKey:kScanVolumeSlider_key];
	[defaults synchronize];
	[self didChangeValueForKey:@"scanVolumeSliderNormalizedValue"];
	
	[self notifyScanVolumeChanges];
}


#pragma mark - Notifications

- (void) notifyScanVolumeChanges
{
	NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
	//[defaultCenter postNotificationName:kSoundVolumeManagerNotification_scanVolumeChanged
								// object:self];
}

@end
