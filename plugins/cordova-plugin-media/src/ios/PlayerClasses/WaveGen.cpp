//---------------------------------------------------------------------------

#include "WaveGen.h"

TWave::TWave(){_init();};
TWave::TWave(int pchan, double pSamp){_init(); chan=pchan; Samp=pSamp; };
TWave::~TWave()
{
//    NiceDel(fsL);    NiceDel(fsR);    NiceDel(fsA);
//    NiceDel(fsBase); NiceDel(fsDim);
}

void TWave::_init(void)
{ n=0; chan=0; Rphase=0; fsA=fsL=fsR=fsBase=fsDim=NULL; Sinc=Ainc=NULL;
    fade_factor=-0.2; cycle=5; Ccycle=0;
}

void TWave::SetDif(double prPH)
{
    Rphase=prPH;
}

void TWave::SetL(double *Pamp, double *PHz, double *Pphase, int nv)
{
    n=nv;
    amp=Pamp; Hz=PHz; phase=Pphase; t=0; inc=*Hz * 2.*M_PI/Samp;
//    NiceDel(fsL);
    fsL=new TFastSin[n];
    for (int i=0; i<n; i++) {
        fsL[i].init(amp[i], Hz[i],      Samp, (phase)?phase[i]:0);
    }
}

void TWave::SetR(double *Pamp, double *PHz, double *Pphase, int nv)
{
    n=nv;
    amp=Pamp; Hz=PHz; phase=Pphase; t=0; inc=*Hz * 2.*M_PI/Samp;
    if (fsR!=NULL) delete[]fsR;
    fsR=new TFastSin[n];
    for (int i=0; i<n; i++) {
        fsR[i].init(amp[i], Hz[i],        Samp, (phase)?phase[i]:0);
    }
}

void TWave::SetAmp(double *Pamp, int chan)
{
    amp=Pamp;
    for (int i=0; i<n; i++) {
        if (chan&CHAN_LEFT ) fsL[i].SetAmp(amp[i]);
        if (chan&CHAN_RIGHT) fsR[i].SetAmp(amp[i]);
    }
}

void TWave::SetAmp(double *Pamp)
{
    amp=Pamp;
    for (int i=0; i<n; i++) {
        fsL[i].SetAmp(amp[i]);
        fsR[i].SetAmp(amp[i]);
    }
}

// AM modulate amp using Hz freq in octave 'oct'
void TWave::AM_Modulate(int oct)
{
//    NiceDel(fsA);
    fsA=new TFastSin[n];
    // convert to oct
    for (int i=0; i<n; i++)
        fsA[i].init(amp[i], FreqInOctave(Hz[i],oct),        Samp, 0);
}

void TWave::Pulse(double hz)
{
//    NiceDel(fsA);
    fsA=new TFastSin[n];
    //
    for (int i=0; i<n; i++)
        fsA[i].init(amp[i], hz, Samp, 0);
}

void TWave::Set(double *Pamp, double *PHz, double *Pphase, int nv)
{
    n=nv;
    amp=Pamp; Hz=PHz; phase=Pphase;
    
    //    NiceDel(fsL); NiceDel(fsR); NiceDel(Sinc); NiceDel(Ainc);
    fsL = NULL; fsR = NULL; Sinc = NULL;Ainc = NULL;
    fsL=new TFastSin[n]; fsR=new TFastSin[n];
    Sinc=new double[n];  Ainc=new double[n];
    Ccycle=0;
    
    for (int i=0; i<n; i++) {
        fsL[i].init(amp[i], Hz[i],        Samp, (phase)?phase[i]:0);
        fsR[i].init(amp[i], Hz[i]+Rphase, Samp, (phase)?phase[i]:0);
        Sinc[i]=1*(2*M_PI/Samp);
        Ainc[i]=0;
    }
}
// only 1 pitch with am+fm combinated effect
// sin(pulse*t) * sin(pitch*t + sin(2*t*pulse))
//    AM              pitch          FM
void TWave::Set(double*Pamp, double*PHz, double Ppulse)
{
    amp=Pamp; Hz=PHz; pulse=Ppulse;
    t=0; inc=*Hz * 2.*M_PI/Samp; n=1;
    
//    NiceDel(fsL); NiceDel(fsR);
    fsL=new TFastSin[1]; fsR=new TFastSin[1];
    
    fsL[0].init(*amp, pulse ,  Samp, 0); // AM component
    fsR[0].init(1   , pulse*2, Samp, 0); // FM component
}

void TWave::gen(short int*sbuff, int sb)
{
    if (chan==2) { // stereo
        for (int i=0; i<sb; i+=2) { // +2 stereo device
            double yL=0, yR=0;
            for (int j=0; j<n; j++) {
                yL+=fsL[j].calc();
                yR+=fsR[j].calc();
            } yL/=n; yR/=n;
            sbuff[i+0] =(short)yL;
            sbuff[i+1] =(short)yR;
        }
    }
}

void TWave::genAM(short int*sbuff, int sb)
{
    if (chan==2) { // stereo
        for (int i=0; i<sb; i+=2) { // +2 stereo device
            
            for (int j=0; j<n; j++) {
                fsL[j].SetAmp(fsA[j].calc());
                fsR[j].SetAmp(fsA[j].calc());
            }
            double yL=0, yR=0;
            for (int j=0; j<n; j++) {
                yL+=fsL[j].calc();
                yR+=fsR[j].calc();
            } yL/=n; yR/=n;
            sbuff[i+0] =(short)yL;
            sbuff[i+1] =(short)yR;
        }
    }
}

void TWave::genAMFM(short int*sbuff, int sb)
{
    if (chan==2) { // stereo
        for (int i=0; i<sb; i+=2) { // +2 stereo device
            double yL;
            yL=fsL[0].calc() * sin( t + fsR[0].calc() );
            t+=inc;
            sbuff[i+0] =(short)yL;   sbuff[i+1] =(short)yL;
        }
    }
}

//---------------------------------------------------------------------------
// bowl support
// amp=volume
// w=main bowl tone
// dim=dim factor < 1
// osc=osc freq.
// nh=# harmonics
void TWave::Bowl(double Pamp, double w, double dim, double osc, int nh, bool vibrato)
{
#define PHI 1.618033989
    n=nh*2; // one for octave and other for PHI related
    
    
    //    NiceDelNew(fsBase,n);
    //    NiceDelNew(fsDim,n);
    
    fsBase = new TFastSin[n];
    fsDim = new TFastSin[n];
    // bowl furmulae
    // y=dim * sin(w*t) + (sin(w*t+PI/2) * sin(osc * t)
    // vibrato in 'w' signal is sin(xt+sin(8*PHI*t)), 8*PHI Hz seems best vibrato value
    n=nh; _amp=Pamp;
    fsOsc.init(1, osc, Samp, 0);
    fsVibrato.init((vibrato)?0.5:0, 7*PHI/2, Samp, 0);
    for (int i=0; i<n; i+=2) {
        fsDim [i].init(dim,  w * pow(2,i/2-nh/2), Samp,      0);
        fsBase[i].init(1,    w * pow(2,i/2-nh/2), Samp, M_PI/2);
        fsDim [i+1].init(dim,  w * pow(2,i/2-nh/2) * PHI, Samp,      0);
        fsBase[i+1].init(1,    w * pow(2,i/2-nh/2) * PHI, Samp, M_PI/2);
    }
}

// singing bels effect
//  Sum( amp[i] * sin(hz[i] * t) * sin( hz[i] / (t+ofs[i]) )
//  to be used with few freqs (max 4)
void TWave::genSingingBell(short int*sbuff, int sb)
{
    if (chan==2) { // stereo
        for (int i=0; i<sb; i+=2) { // +2 stereo device
            double yL=0,yR=0;
            for (int j=0; j<n; j++) {
                double ph=((phase)?phase[j]:6);
                yL+=fsL[j].calc()*sin(Hz[j]/(Ainc[j]+ph));
                yR+=fsR[j].calc()*sin(Hz[j]/(Ainc[j]+ph));
                Ainc[j]+=Sinc[j];
            } yL/=n; yR/=n;
            
            sbuff[i+0] =(short)yL;
            sbuff[i+1] =(short)yR;
        }
    }
}

// faded effect, should be re-started (Ainc[j]=0) after selected duration
void TWave::genFaded(short int*sbuff, int sb)
{
    if (chan==2) { // stereo
        for (int i=0; i<sb; i+=2) { // +2 stereo device
            double yL=0,yR=0;
            for (int j=0; j<n; j++) {
                double ph=((phase)?phase[j]:6);
                yL+=fsL[j].calc()*sin(Hz[j]/(Ainc[j]+ph))*exp( fade_factor * Ainc[j]);
                yR+=fsR[j].calc()*sin(Hz[j]/(Ainc[j]+ph))*exp( fade_factor * Ainc[j]);
                Ainc[j]+=Sinc[j];
            } yL/=n; yR/=n;
            
            sbuff[i+0] =(short)yL;
            sbuff[i+1] =(short)yR;
        }
    }
}


// bowl furmulae with vibrato
// y=dim * sin(w*t + sin(8�phi�t) + (sin(w*t+PI/2) * sin(osc * t)
void TWave::genBowl(short int*sbuff, int sb)
{
    if (chan==2) { // stereo
        for (int i=0; i<sb; i+=2) { // scale to max
            double v=0;
            for (int j=0; j<n; j++) {
                v+=fsDim[j].calcSin(fsVibrato.calc()) + (fsBase[j].calc() * fsOsc.calc());
            }
            v/=n;
            double yL=_amp * v;
            sbuff[i+0] =(short)yL;
            sbuff[i+1] =(short)yL; // R=L
        }
    }
}

void TWave::ResetTime(void)
{
    if (Ainc && n) memset(Ainc, 0, sizeof(*Ainc)*n);
}

void TWave::ResetTime(double elap)
{
    if (Ainc && n)
        if (elap>Ccycle) { memset(Ainc, 0, sizeof(*Ainc)*n); Ccycle+=cycle; }
}

