//
//  oalQueuePlayback.m
//  AuraApp
//
//  Created by Svyatoshenko "Megal" Misha on 25.10.13.
//  Copyright (c) 2013 Quantum Life LLC. All rights reserved.
//

#import "oalQueuePlayback.h"

#import "MyOpenALSupport.h"
#import <OpenAL/al.h>
#import <OpenAL/alc.h>
#import "MyOpenALSupport.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioServices.h>
#import <AudioToolbox/AudioToolbox.h>
#import "SoundVolumeManager.h"

const size_t kOpenALQueuePlayerBufferCount = 5;
static OSStatus kAlreadyInit = 'init';


void interruptionListenerCallbackOalQueuePlayer( void *inUserData, UInt32 interruptionState )
{
	oalQueuePlayback *player = [oalQueuePlayback sharedInstance];
	switch( interruptionState )
	{
		case kAudioSessionBeginInterruption:
		{
			[player beginInterruption];
			break;
		}
		case kAudioSessionEndInterruption:
		{
			[player endInterruptionWithFlags:AVAudioSessionInterruptionFlags_ShouldResume];
			break;
		}
		default:break;
	}
}

@interface oalQueuePlayback (/* Private */)
{
	ALCcontext				*_context;
	ALCdevice				*_device;
	ALuint		            _buffers[5];
	ALuint					_source;
}

//! array with indexes from set { 0..kOpenALQueuePlayerBufferCount-1 }, that represents free buffers from _buffers / _dataBuffers arrays.
@property( nonatomic, strong ) NSMutableIndexSet	*setBufferFreeIndexes;
@property( nonatomic, assign ) BOOL					isPaused;

- (BOOL) setupAudioSession;
- (BOOL) setupOpenAL;
- (BOOL) setupListener;

@end

/**********************
 *                    *
 *         [S]        *
 *          V         *
 *                    *
 *          ^         *
 *         [L]        *
 *                    *
 **********************/

@implementation oalQueuePlayback

-(void)dealloc
{
	[self teardownOpenAL];

    [[NSNotificationCenter defaultCenter] removeObserver:self];
	[self teardownAudioSession];
}

- (void)teardownOpenAL
{
	// Delete the Sources
	if( _source )
	{
		alDeleteSources(1, &_source);
	}
	// Delete the Buffers
    alDeleteBuffers(kOpenALQueuePlayerBufferCount, _buffers);
	
    //Release context
    alcMakeContextCurrent(NULL);
    alcDestroyContext(_context);
    _context = NULL;
    //Close device
    alcCloseDevice(_device);
    _device = NULL;
}

+ (instancetype) sharedInstance
{// could be nul if error has accured during initialization. Will re-try initialization next time
	static oalQueuePlayback *sharedSelf;
	@synchronized( self )
	{
		if( !sharedSelf )
		{
			sharedSelf = [[oalQueuePlayback alloc] init];
		}
		return sharedSelf;
	}
}

- (id) init
{
	self = [super init];
	if( self )
	{
		memset(_buffers, 0, sizeof(_buffers));
		_setBufferFreeIndexes = [NSMutableIndexSet indexSetWithIndexesInRange:NSMakeRange(0, kOpenALQueuePlayerBufferCount)];
		self.isPaused = NO;
		
		if( ![self setupAudioSession] )
		{
			return nil;
		}
		if( ![self setupOpenAL] )
		{
			[self teardownAudioSession];
			return nil;
		}
		if( ![self setupSource] )
		{
			[self teardownOpenAL];
			[self teardownAudioSession];
			return nil;
		}
		if( ![self setupListener] )
		{
			[self teardownOpenAL];
			[self teardownAudioSession];
			return nil;
		}
		
		[self updateVolume];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateVolume) name:kSoundVolumeManagerNotification_scanVolumeChanged object:nil];
	}
	
	return self;
}

- (BOOL) setupAudioSession
{
	
	OSStatus result;
	result = AudioSessionInitialize(NULL, NULL, interruptionListenerCallbackOalQueuePlayer, NULL);
	if( result != kAudioSessionNoError && result != kAlreadyInit )
	{
		NSLog( @"Error setting up AudioSession.");
		return NO;
	}
	
	UInt32 category = kAudioSessionCategory_MediaPlayback;
	result = AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(category), &category);
	if( result != kAudioSessionNoError )
	{
		NSLog( @"Error setting up AudioSession.");
		return NO;
	}

	result = AudioSessionSetActive(YES);
	if( result != kAudioSessionNoError )
	{
		NSLog( @"Error setting up AudioSession.");
		return NO;
	}
	
	return YES;
}

- (void) teardownAudioSession
{
	// Deactivate the current audio session
	AudioSessionSetActive(NO);
}

- (BOOL) setupOpenAL
{
	ALenum			error;
    
	// Create a new OpenAL Device
	// Pass NULL to specify the system’s default output device
	_device = alcOpenDevice(NULL);
	if( _device != NULL )
	{
		// Create a new OpenAL Context
		// The new context will render to the OpenAL Device just created
		_context = alcCreateContext(_device, 0);
		if( _context != NULL )
		{
			// Make the new context the Current OpenAL Context
			alcMakeContextCurrent(_context);
			
			// Create some OpenAL Buffer Objects
			alGenBuffers(kOpenALQueuePlayerBufferCount, _buffers);
			if( (error = alGetError()) != AL_NO_ERROR )
			{
				NSLog(@"Error Generating Buffers: %x", error);
				return NO;
			}
			
			// Create some OpenAL Source Objects
			alGenSources(1, &_source);
			if(alGetError() != AL_NO_ERROR)
			{
				NSLog(@"Error generating sources! 0x%X\n", error);
				return NO;
			}
			
		}
		else
		{
			alGetError();
			return NO;
		}
	}
	else
	{
		alGetError();
		return NO;
	}
	// clear any errors
	alGetError();
	return YES;
}

- (BOOL) setupSource
{
	ALenum error = AL_NO_ERROR;
	alGetError(); // Clear the error
    
	// Turn Looping OFF
	alSourcei(_source, AL_LOOPING, AL_FALSE);
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error initializing source 0x%X\n", error);
		return NO;
	}
	
	// Set Source Position
    float pan = [[NSUserDefaults standardUserDefaults] floatForKey:@"rightChannelVolume"] - [[NSUserDefaults standardUserDefaults] floatForKey:@"leftChannelVolume"];
    
    float sourcePosAL[] = {pan, 0.0f, 0.0f};
	alSourcefv(_source, AL_POSITION, sourcePosAL);
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error initializing source 0x%X\n", error);
		return NO;
	}
	
	// Set Source Reference Distance
	alSourcef(_source, AL_REFERENCE_DISTANCE, 1.0f);
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error initializing source 0x%X\n", error);
		return NO;
	}
	
	// clear any errors
	return YES;
}

- (BOOL) setupListener
{
	ALenum error = AL_NO_ERROR;
	
	float listenerPosAL[] = {0.0f, 0.0f, 0.0f};
	alListenerfv(AL_POSITION, listenerPosAL);
	
	float ori[] = {0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f};
	alListenerfv(AL_ORIENTATION, ori);
	
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error initializing Listener 0x%X\n", error);
	}
	
	// clear any errors
	return (alGetError() == AL_NO_ERROR);
}

- (void) updateVolume 
{
	float gain = [SoundVolumeManager sharedInstance].scanGain;
    alListenerf(AL_GAIN, gain);
}

#pragma mark - working with buffers

- (BOOL) isNewBufferNeeded
{
	BOOL isSoucreValid = alIsSource(_source);
	if( !isSoucreValid )
	{
		NSLog( @"Source is invalid!"  );
		return NO;
	}
	
	return (self.setBufferFreeIndexes.count>0);
}

- (void) addNewBuffer:(void *)rawData
		   withLength:(size_t)length
		 withChannels:(int)channels
   withBitResoliution:(int)bitResolution
	 withSamplingRate:(int)samplingRate
{
	if( /*channels!=2 ||*/ bitResolution!=16 )
	{
		NSLog( @"Error: buffer with %d channels and %d bitResolution is not supported. try to use stereo, 16bit", channels, bitResolution );
		return;
	}
	
	ALenum  error = AL_NO_ERROR;
	ALenum  format = AL_FORMAT_STEREO16;
	ALsizei size = (ALsizei)length;
	ALsizei freq = samplingRate;

	// get a free buffer;
	NSUInteger bufferIndex = [self.setBufferFreeIndexes lastIndex];
	[self.setBufferFreeIndexes removeIndex:bufferIndex];
//	NSLog( @"INFO: will queue buffer %d at index %d", _buffers[bufferIndex], bufferIndex );

	// use a copy buffer
	alBufferData(_buffers[bufferIndex], format, rawData, size, freq);
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error attaching audio to buffer: 0x%X\n", error);
	}
	
	// attach OpenAL Buffer to OpenAL Source
	alSourceQueueBuffers(_source, 1, &_buffers[bufferIndex]);
	if((error = alGetError()) != AL_NO_ERROR) {
		NSLog(@"Error attaching buffer to source: 0x%X\n", error);
		exit(1);
	}
	
	// chech if player has been stopped
	ALenum sourceState;
	alGetSourcei(_source, AL_SOURCE_STATE, &sourceState);
	if( !sourceState != AL_STOPPED )
	{
		[self startPlaying];
	}
	
	[self updateVolume];
	return;
}

- (void) addNewBuffer:(NSURL *)urlToWavFile
{
	// use local static buffers for NSURL playback
	typedef void *pvoid_t;
	static pvoid_t _dataBuffers[kOpenALQueuePlayerBufferCount];

	ALenum  error = AL_NO_ERROR;
	ALenum  format;
	ALsizei size;
	ALsizei freq;
	
	CFURLRef fileURL = (CFURLRef)CFBridgingRetain(urlToWavFile);
	if( fileURL )
	{
		// get a free buffer;
		NSUInteger bufferIndex = [self.setBufferFreeIndexes lastIndex];
		[self.setBufferFreeIndexes removeIndex:bufferIndex];
//		NSLog( @"INFO: will queue buffer %d at index %d", _buffers[bufferIndex], bufferIndex );

		
		_dataBuffers[bufferIndex] = (void *)MyGetOpenALAudioData(fileURL, &size, &format, &freq);
		CFRelease(fileURL);
		if( (error = alGetError()) != AL_NO_ERROR )
		{
			NSLog(@"error loading sound: 0x%X\n", error);
			exit(1);
		}
		
		// use the static buffer data API
		alBufferDataStaticProc(_buffers[bufferIndex], format, _dataBuffers[bufferIndex], size, freq);
		if( (error = alGetError()) != AL_NO_ERROR )
		{
			NSLog(@"error attaching audio to buffer: 0x%X\n", error);
		}

		// attach OpenAL Buffer to OpenAL Source
		alSourceQueueBuffers(_source, 1, &_buffers[bufferIndex]);
		if((error = alGetError()) != AL_NO_ERROR) {
			NSLog(@"Error attaching buffer to source: 0x%X\n", error);
			exit(1);
		}
		
		// will start playing ONLY if it is not playing now.
		[self startPlaying];
	}
	else
	{
		NSLog( @"couldn't open file");
		return;
	}
}

- (void) stopPlaying
{
	BOOL isSoucreValid = alIsSource(_source);
	if( !isSoucreValid )
	{
		NSLog( @"Source is invalid!"  );
		return;
	}
	
	ALenum error;

	alSourcePause(_source);
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error stoping source: 0x%X\n", error);
		return;
	}
	
	[self cleanupProcessedBuffers];
}

- (BOOL) startPlaying
{
	BOOL isSoucreValid = alIsSource(_source);
	if( !isSoucreValid )
	{
		NSLog( @"Source is invalid!"  );
		return NO;
	}
	
	if( self.setBufferFreeIndexes.count > 0 )
	{
		return NO;
	}
	
	ALenum sourceState;
	alGetSourcei(_source, AL_SOURCE_STATE, &sourceState);
	if( sourceState == AL_PLAYING )
	{ // already playing
		return NO;
	}
	
	NSLog(@"Start!\n"); // TODO FIX: BUG after 1 stop.
	// Begin playing our source file
	alSourcePlay(_source);

	ALenum error;
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error starting source: 0x%X\n", error);
		return NO;
	}
	
	return YES;
}

- (void) cleanupProcessedBuffers
{
	BOOL isSoucreValid = alIsSource(_source);
	if( !isSoucreValid )
	{
		NSLog( @"Source is invalid!"  );
		return;
	}
	
	ALenum error = AL_NO_ERROR;
	
	ALint buffersProcessed;
	
	alGetSourcei(_source, AL_BUFFERS_PROCESSED, &buffersProcessed);
	if( (error = alGetError()) != AL_NO_ERROR )
	{// an error
		NSLog(@"error Asking Buffers info 0x%X\n", error);
		return ;
	}
	
	if( buffersProcessed<kOpenALQueuePlayerBufferCount/2 )
	{ // don't waste time on cleaning buffers ofthen
		return;
	}
	
	for( int i=0; i<(int)buffersProcessed-1; ++i )
	{
		BOOL isSuccess = [self removeNextBuffer];
		if( !isSuccess ) break;
	}
}

- (BOOL) removeNextBuffer
{
	ALenum error = AL_NO_ERROR;
	ALuint bufferID;
	alSourceUnqueueBuffers(_source, 1, &bufferID);
	
	if( (error = alGetError()) != AL_NO_ERROR )
	{// an error
		NSLog(@"error Unqueueing buffer 0x%X", error);
		return NO;
	}

	
	for( NSUInteger bufferIndex = 0; bufferIndex<kOpenALQueuePlayerBufferCount; ++bufferIndex )
	{
		if( bufferID==_buffers[bufferIndex] )
		{
			[self.setBufferFreeIndexes addIndex:bufferIndex];
			return YES;
		}
	}
	
	NSLog( @"WARNING: buffer with bufferID %d wasn't find", (int)bufferID );
	return YES;
}

- (void) logSourceInfo
{
	ALenum error = AL_NO_ERROR;
	ALint buffersProcessed;

	alGetSourcei(_source, AL_BUFFERS_PROCESSED, &buffersProcessed);
	if( (error = alGetError()) != AL_NO_ERROR )
	{
		NSLog(@"error Asking Buffers info 0x%X\n", error);
	}
}

// Handle audio interruption
- (void) interruption:(NSNotification*)notification
{
	NSDictionary *interruptionDict = notification.userInfo;
	
	NSUInteger interruptionType;
	interruptionType = [[interruptionDict valueForKey:AVAudioSessionInterruptionTypeKey] integerValue];
	
	if (interruptionType == AVAudioSessionInterruptionTypeBegan){
		[self beginInterruption];
	}
	else if (interruptionType == AVAudioSessionInterruptionTypeEnded){
		[self endInterruptionWithFlags:AVAudioSessionInterruptionFlags_ShouldResume];
	}
}

#pragma mark - AVAudioSessionDelegate

- (void)beginInterruption
{
	[self stopPlaying];
	
	if( _context == alcGetCurrentContext() )
	{
		alcMakeContextCurrent(NULL);
	}
	alcSuspendContext(_context);

 	AudioSessionSetActive(NO);
	self.isPaused = YES;
}

- (void)endInterruptionWithFlags:(NSUInteger)flags
{
 	AudioSessionSetActive(YES);
	NSLog( @"flags : %d", flags);
	
	__typeof( self ) __weak me = self;
	double delayInSeconds = 2.0;
	dispatch_time_t popTime;
	popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
	dispatch_after(popTime, dispatch_get_current_queue(), ^(void)
	{
		__typeof( self ) __strong myself = me;
		OSStatus error = AudioSessionSetActive(YES);
		BOOL isRecreatedOk = ( error&kAudioSessionNoError || error&kAlreadyInit );
		if( !isRecreatedOk )
		{
			NSLog( @"Some error has occured.");
		}

		alcMakeContextCurrent(myself->_context);
		alcProcessContext(myself->_context);
		self.isPaused = NO;
	});
}

@end






