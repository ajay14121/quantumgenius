angular.module('aura-genie.controllers').controller('accessoriesCtrl', function($scope,$localStorage,$rootScope) {

    $scope.data = {};
    if(!$localStorage.settings){
        console.log('settings already not defined');
        $localStorage.settings = {
            volumeLeft :50,
            volumeRight :50
        }
    }
    $scope.data.volumeLeft  = $localStorage.settings.volumeLeft;
    $scope.data.volumeRight = $localStorage.settings.volumeRight;

   // $scope.data.volumeLeft  = 60;
   // $scope.data.volumeRight = 60;

    $scope.leftVol= function(){
       console.log("left :-" + $scope.data.volumeLeft);
       $localStorage.settings.volumeLeft = $scope.data.volumeLeft;
       $scope.defaultAudio = "beep.wav";
       if($rootScope.platform == 'android'){
            $scope.defaultAudio = "/android_asset/www/beep.wav";
       }
       var my_media = new Media($scope.defaultAudio, function(e){
            console.log('sound playing');
       }, function(e){
            console.log('error sound playing');
            console.log(e);
       });
       my_media.setLeftVolume($scope.data.volumeLeft);

    }
    

     $scope.rightVol= function(){
         console.log("right :-" + $scope.data.volumeRight);
         $localStorage.settings.volumeRight = $scope.data.volumeRight;
         $scope.defaultAudio = "beep.wav";
         if($rootScope.platform == 'android'){
              $scope.defaultAudio = "/android_asset/www/beep.wav";
        }
         var my_media = new Media($scope.defaultAudio, function(e){
              console.log('sound playing');
         }, function(e){
              console.log('error sound playing');
              console.log(e);
         });

        my_media.setRightVolume($scope.data.volumeRight);
    };

});


    