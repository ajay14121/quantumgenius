angular.module('aura-genie.controllers').controller('accountActivationCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage,$cordovaEmailComposer) {
    $scope.data = {};
    $scope.data.email = '';
    $scope.data.activation_key = '';
    $scope.status = $localStorage.user.status;
    $scope.show= false;
    $scope.accountActivation = function() {
//       if ($scope.data.email == '') {
//            $ionicPopup.alert({
//                title: 'Empty Field',
//                template: 'Email is mandatory'
//            });            
//        } else if (!$rootScope.email_filter.test($scope.data.email)) {
//            $ionicPopup.alert({
//                title: 'Invalid Email',
//                template: 'Please enter valid email'
//            });
//        }else 
          if ($scope.data.activation_key == '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Activation code is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/account_activation",
                method: 'POST',
                data: {
                   // email: $scope.data.email,
                    activation_key: $scope.data.activation_key,
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Activation Completed",
                        template: response.message
                    }).then(function(res) {
                        $location.path("/app/user_record_one");
                    });
                } else {
                    $scope.show= true;
                    $ionicPopup.alert({
                        title: "Activation Failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Activation Process Failed',
                    template: 'API is not working'
                });
            });
        }
    }
    
     $scope.emailSupport= function(){
        $cordovaEmailComposer.isAvailable().then(function() {
            // is available
            console.log("email composer available");
        }, function () {
          // not available
          console.log("email composer not available");
        });

        var email = {
            to: 'quantumliferyan@gmail.com',
            subject: 'Quantum Genius Activation Support Request',
            body: "My Quantum Genius Activation Code is not working. My details are as follows: <br> \n\
                   Please have someone contact me immediately to resolve this issue.  <br> \n\
                   Thank you.",
            isHtml: true
        };
        $cordovaEmailComposer.open(email).then(null, function () {
            // user cancelled email
            console.log("user cancelled email");
        });      
    };
});
    
