angular.module('aura-genie.controllers').controller('addLibraryCtrl', function($scope,$http,$rootScope,$cordovaCamera,$state,$ionicLoading,$ionicPopup,$localStorage,$stateParams,$cordovaFileTransfer,$http) {
    $scope.data = {};
    $scope.data.name = '';
    $scope.data.description = '';
    $scope.data.image = '';
    $scope.data.record_id = '';
    $rootScope.library_user = {};
    $rootScope.library_user.id = '';
    $scope.selectImage= function(){
        try {
            var options = {
                quality: 50,
                destinationType: Camera.DestinationType.FILE_URI,
                sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit: true,
                targetWidth: 200,
                targetHeight: 200

            };

            $cordovaCamera.getPicture(options).then(function(libraryImageURI) {
                console.log('Camera getPicture Success');
                console.log(libraryImageURI);
                $scope.libraryImageURI = libraryImageURI;
               
            }, function(err) {
                // error
                console.log('Camera getPicture Error');
                console.log(err);
            });
        } catch (e) {
            console.log('Camera Plugin only work on device');
        }
    };
    
    $scope.userList = function(){
        $state.go("app.list_user");  
    };
    
    $scope.addLibrary= function(){
        $scope.uploadImage();
    };
    
     $scope.uploadImage = function() {
        var server = $rootScope.apiUrl + "API/User/upload_file";
        
        var filePath = $scope.libraryImageURI;
        var trustHosts = true;
        var options = {
            params: {
                type: 'image',
                file:$scope.libraryImageURI
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            }
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);
                        console.log('file upload success');
                        console.log(response);
                        $ionicLoading.hide();
                        if (response.status) {
                            $scope.data.image = response.data.filename;
                            $scope.addLibraryRecord();
                        } else {
                            $ionicPopup.alert({
                                title: 'Upload Image Failed Temporary',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                       console.log('file upload error');
                        console.log(err);
                        var alertPopup = $ionicPopup.alert({
                            title: 'API Error',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        //$ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Tarfer Plugin will work on device');
            console.log(e);
        }
    };

    $scope.addLibraryRecord = function(){
        $scope.data.record_id = $rootScope.library_user.id;
        console.log($scope.data);   
        if($scope.data.name == '')
        {
            $ionicPopup.alert({
                    title: "Invalid Category Name ",
                    template: "Category name is mandatory"
                });
        }else if (!$rootScope.name.test($scope.data.name.charAt(0))) {
            $ionicPopup.alert({
                title: 'Invalid Category Name',
                template: 'First charcter should be alphabet'
            });
        }else if (!$rootScope.item_added.test($scope.data.name)) {
            $ionicPopup.alert({
                title: 'Invalid Category Name',
                template: 'Only alphanumeric characters are allowed'
            });
        }else if($scope.data.description=='')
        {
             $ionicPopup.alert({
                    title: "Invalid Description ",
                    template: "Description is mandatory"
                });
        }
        else if (!$rootScope.item_added.test($scope.data.description)) {
            $ionicPopup.alert({
                title: 'Invalid Description',
                template: 'Only alphanumeric characters are allowedd'
            });
        }else{
        $http({
            url: $rootScope.apiUrl + "API/User_library/add",
            method: 'POST',
            data:{
                    record_id: $scope.data.record_id,
                    name: $scope.data.name,
                    description: $scope.data.description,
                    image:$scope.data.image
        } ,
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status) {
                $ionicPopup.alert({
                    title: "Success",
                    template: response.message
                }).then(function(res) {
                   $state.go("app.libraries");
                });
                
                
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'API Error',
                template: 'API is not working'
            });
        });
    }
    };
});
