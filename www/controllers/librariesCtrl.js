angular.module('aura-genie.controllers').controller('librariesCtrl', function($scope, $state, $rootScope, $ionicLoading, $localStorage, $ionicPopup, $http) {
    $scope.data = {};
    $scope.data.library_list = '';
    $scope.categories = '';
    $rootScope.library_detail = {};
    $rootScope.library_detail.name = '';

    $ionicLoading.show();
    $http({
        url: $rootScope.apiUrl + "API/User_library/list",
        method: 'POST',
        headers: {
            'Access-Token': $localStorage.user.token,
            'Email': $localStorage.user.email
        }
    }).success(function(response) {
        $ionicLoading.hide();
        if (response.status) {
            $scope.data.library_list = response.data;
        } else {
            $ionicPopup.alert({
                title: "Error",
                template: response.message
            });
        }
    }).error(function(erorr) {
        $ionicLoading.hide();
        $ionicPopup.alert({
            title: 'API/User/list_user_record',
            template: 'API is not working'
        });
    });

    $scope.addLibrary = function() {
        $state.go("app.add_library");
    };

    $scope.showLibraryDetail = function(library_id, library_name) {
        $state.go("library_detail",{id:library_id,name:library_name});
    };
});
    