angular.module('aura-genie.controllers').controller('libraryDetailCtrl', function($scope, $ionicLoading, $localStorage, $state, $rootScope, $cordovaFileTransfer, $http, $ionicPopup, $stateParams, $cordovaCamera) {

    $scope.data={};
    $scope.data.name = '';
    $scope.email_address='';
    $scope.edit = false;
    $scope.readonly = true;
    $scope.edit_disabled = false;
    $scope.data.description = '';
    $scope.data.image = '';
    $scope.data.record_id = '';
    $scope.data.library_id = '';
    $scope.showSave = false;

    $rootScope.library_detail = {};
    $scope.library = {
        id:$stateParams.id,
        name: $stateParams.name
    };
console.log($stateParams);

    $scope.loadDetail = function(){
             $ionicLoading.show();
                $http({
                    url: $rootScope.apiUrl + "API/User_library/detail",
                    method: 'POST',
                    data:   {
                        library_id: $scope.library.id
                    },
                    headers: {
                        'Access-Token': $localStorage.user.token,
                        'Email': $localStorage.user.email
                    }
                }).success(function(response) {
                    $ionicLoading.hide();
                    if (response.status) {
                        $scope.data= response.data;

                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: response.message
                        });
                    }
                }).error(function(erorr) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({
                        title: 'API/User/list_user_record',
                        template: 'API is not working'
                    });
                });
    };
    
    $scope.editLibrary = function() {
        $scope.edit = true;
        $scope.readonly = false;
        $scope.showSave = true;
        $scope.edit_disabled = true;
    };

    $scope.changeImage = function() {
        if ($scope.edit) {
            try {
                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
                    allowEdit: true,
                    targetWidth: 200,
                    targetHeight: 200
                };
                $cordovaCamera.getPicture(options).then(function(imageURI) {
                    console.log('Camera getPicture Success');
                    console.log(imageURI);
                    $scope.imageURI = imageURI;
                }, function(err) {
                    // error
                    console.log('Camera getPicture Error');
                    console.log(err);
                });
            } catch (e) {
                console.log('Camera Plugin only work on device');
            }
        }
    };

    $scope.changeName = function(library_id) {
        console.log(library_id);
        if ($scope.edit) {
            $state.go("app.list_user", {id: library_id});
        }
    };

    $scope.addItems = function() {
        $state.go("app.library_items", {library_id: $scope.library.id, library_name: $scope.library.name}, {reload: true});
    };

    $scope.updateLibrary = function() {
        $scope.uploadImage();
    };

    $scope.uploadImage = function () {
        var server = $rootScope.apiUrl + "API/User/upload_file";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";
        console.log($scope.data.image);
        var filePath = $scope.imageURI;
        var trustHosts = true;
        var options = {
            params: {
                //  record_id: $stateParams.user_record_id,
                type: 'image',
                file:$scope.imageURI
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            }
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);
                        console.log('file upload success');
                        console.log(response);
                        $ionicLoading.hide();
                        if (response.status) {
                            $scope.data.image = response.data.filename;
                            $scope.updateLibraryRecord();
                        } else {
                            $ionicPopup.alert({
                                title: 'Upload Image Failed Temporary',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                       console.log('file upload error');
                        console.log(err);
                        var alertPopup = $ionicPopup.alert({
                            title: 'API Error',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        //$ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Tarfer Plugin will work on device');
            console.log(e);
        }
    };

    $scope.updateLibraryRecord = function() {
        $ionicLoading.show();

        $http({
            url: $rootScope.apiUrl + "API/User_library/update",
            method: 'POST',
            data: $scope.data,
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status) {
                $scope.data = response.data;
                $ionicPopup.alert({
                    title: "successful",
                    template: response.message
                }).then(function(abc){
                        $scope.edit = false;
                        $scope.readonly = true;
                        $scope.edit_disabled = false;
                        $scope.loadDetail();
                });
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'API Error',
                template: 'API is not working'
            });
        });
    };
    $scope.exportLibrary = function () {
        $scope.popup = $ionicPopup.show({
            title: 'Export',
            templateUrl: 'templates/form-popup/export_library.html',
            delay: '10000',
            cssClass: 'form-popup add-item',
            scope: $scope,
            buttons: [
                {
                    text: "Ok",
                    onTap: function () {
                        $scope.valiDateEmail();
                        //$scope.exportLibrary();
                    }
                },
                {
                    text: "Cancel"
                }
            ]
        });
    };
    $scope.valiDateEmail = function ()
    {
        if ($scope.email_address=='') {
            $ionicPopup.alert({
                title: 'Empty field',
                template: 'Email and password are mandatory'
            }).then(function () {
                $scope.exportLibrary();
            });
        } else if (!$rootScope.email_filter.test($scope.email_address)) {
            $ionicPopup.alert({
                title: 'Invalid Email',
                template: 'Please enter valid email'
            }).then(function () {
                $scope.exportLibrary();

            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/export_library",
                method: 'POST',
                data: {
                    email:$scope.data.email ,
                    library_id:$scope.library.id
                },
                headers:
                {
                   'Access-Token': $localStorage.user.token,
                   'Email': $localStorage.user.email         
                }
            }).success(function (response) {
                $ionicLoading.hide();
        if (response.status) {
            $ionicPopup.alert({
                title: "Success",
                template:"library exported successfully"
            });
        } else {
            $ionicPopup.alert({
                title: "Error",
                template: response.message
            });
        }
            }).error(function (error) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                   title: 'API/User/export_library',
                   template: 'API is not working'
                });
            });
        }
    };
    $scope.loadDetail();
});
