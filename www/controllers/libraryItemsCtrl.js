angular.module('aura-genie.controllers').controller('libraryItemsCtrl',function($scope,$ionicPopup,$ionicLoading,$http,$localStorage,$rootScope,$stateParams,$state) {
    $scope.data = {};
    $scope.data.frequencies = [];
    $scope.data.item_name='';
    $scope.invalid_frequency= '';
    $scope.library = {
        id:$stateParams.library_id,
        name: $stateParams.library_name
    };
    
    $scope.loadItems = function(){
        
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User_library/list_items",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            },
            data:{
                 library_id: $scope.library.id
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status) {
                $scope.data.library_items = response.data;
                console.log($scope.data.library_items);
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'API Error',
                template: 'API is not working'
            });
        });
    };
    
    $scope.addManually = function(){
        $scope.popup = $ionicPopup.show({
            title: 'Add Item',
            templateUrl: 'templates/form-popup/add_item.html',
            delay: '10000',
            cssClass: 'form-popup add-item',
            scope: $scope,
            buttons: [
                {
                    text: "Repeat",
                    onTap: function (e) {
                            console.log($scope.data);
                              $scope.addItem();   
                              $scope.addManually();
                    }
                }, {
                    text: "Continue",
                    onTap: function (e) {
                        $scope.addItem();
                       $state.go("app.library_items"); 
                        
                    }
                }
            ]
        });
    };
    
    
    $scope.addItem = function(){
        $scope.invalid_frequency='';
        console.log($scope.data.frequencies.length);
        if($scope.data.frequencies.length==0){
                $scope.invalid_frequency='blank';
                console.log( $scope.invalid_frequency);
            }else{
                for(i=0;i<$scope.data.frequencies.length;i++){
            if($scope.data.frequencies[i]>20000 || $scope.data.frequencies[i]<0)
            {
                console.log("in if");
                $scope.invalid_frequency='greaterOrLess';
                
            }else if (!$rootScope.digit.test($scope.data.frequencies[i])) {
                   console.log($scope.data.frequencies[i]);
                   if($scope.data.frequencies[i]== undefined)
                   {
                        $scope.invalid_frequency='';
                   }else
                   {
                       $scope.invalid_frequency='notDigit';  
                   }                 
            }
            }
           }
        
        
        
        if($scope.data.item_name==''){
            $ionicPopup.alert({
                title: "Invalid Item Name",
                template: "Item name is mandatory"
            }).then(function(res){
                $scope.addManually();
            });
        }else if (!$rootScope.item_added.test($scope.data.item_name)) {
             $ionicPopup.alert({
                title: "Invalid Item Name",
                template: "Only alphanumeric characters are allowed"
            }).then(function(res){
                $scope.addManually();
            });
        }else if (!$rootScope.name.test($scope.data.item_name.charAt(0))) {
            $ionicPopup.alert({
                title: 'Invalid Category Name',
                template: 'First charcter should be alphabet'
            });
        }else if ($scope.invalid_frequency == 'blank') {
             $ionicPopup.alert({
                title: "Invalid Frequency",
                template: " Frequency is mandatory"
            }).then(function(res){
                $scope.addManually();
            });
        }else if ($scope.invalid_frequency == 'greaterOrLess') {
             $ionicPopup.alert({
                title: "Invalid Frequency",
                template: "All frequency values must be in range of {0,20000}"
            }).then(function(res){
                $scope.addManually();
            });
        }else if ($scope.invalid_frequency == 'notDigit') {
             $ionicPopup.alert({
                title: "Invalid Frequency",
                template: "Enter only numeric value"
            }).then(function(res){
                $scope.addManually();
            });
        }else{
            console.log($scope.data);
        $scope.data.library_id = $scope.library.id;
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User_library/add_item",
            method: 'POST',
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            },
            data:
                    {
                        library_id:$scope.library.id,
                        name:$scope.data.item_name,
                        frequency:$scope.data.frequencies
                    }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status) {
                $ionicPopup.alert({
                title: "Successful",
                template: response.message
            }).then(function(res){
                $scope.data.frequencies=[];
                $scope.data.item_name='';
                $scope.loadItems();
            });
               
            } else {
                $ionicPopup.alert({
                    title: "Error",
                    template: response.message
                }).then(function(res){
                    $scope.addManually();
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'API Error',
                template: 'API is not working'
            });
        });
        }
    };
    $scope.loadItems();

       
});
    