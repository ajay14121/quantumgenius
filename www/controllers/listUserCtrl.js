angular.module('aura-genie.controllers').controller('listUserCtrl', function($scope,$rootScope,$ionicLoading,$ionicPopup,$http,$localStorage,$state,$stateParams,$ionicHistory) {
    $scope.data = {};
    $scope.data.userList = '';
    $scope.library={};
    $scope.library.id= $stateParams.id;
    console.log($scope.library.id);
    $ionicLoading.show();
    $http({
        url: $rootScope.apiUrl + "API/User/list_user_record",
        method: 'POST',
        headers: {
            'Access-Token': $localStorage.user.token,
            'Email': $localStorage.user.email
        }
    }).success(function(response) {
        $ionicLoading.hide();
        if (response.status) {
            $scope.data.userList = response.data;
        } else {
            $ionicPopup.alert({
                title: "Error",
                template: response.message
                
            });
        }
    }).error(function(erorr) {
        $ionicLoading.hide();
        $ionicPopup.alert({
            title: 'API/User/list_user_record',
            template: 'API is not working'
        });
    });
    
    $scope.selectUser = function(id,username){
        if($scope.library.id){
            $rootScope.library_detail.id= id;
            $rootScope.library_detail.name= username;
            
            $ionicHistory.goBack();
            //$state.go("library_detail"); 
        }
        else{
        $rootScope.library_user.id= id;
        $rootScope.library_user.name= username;
        $state.go("app.add_library"); 
        console.log("user name is :- " +$rootScope.library_user.name);
        console.log("user id is :- " + $rootScope.library_user.id);
        }
        
    };
});