angular.module('aura-genie.controllers').controller('otpCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading,$localStorage,$state,$ionicHistory) {
    $scope.data = {};
    $scope.data.email = '';
    $scope.data.otp = '';
    $scope.confirmOTP = function() {
        if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Email is mandatory'
            });
        } else if (!$rootScope.email_filter.test($scope.data.email)) {
            $ionicPopup.alert({
                title: 'Invalid Email',
                template: 'Your email id is not valid'
            });
        } else if ($scope.data.otp == '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'OTP is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/confirm_otp",
                method: 'POST',
                data: {
                    email: $scope.data.email,
                    otp: $scope.data.otp,
                    uuid: $rootScope.uuid,
                    platform: $rootScope.platform
                }
            }).success(function(response) {
                $ionicLoading.hide();
                $localStorage.user = response.data;
                $rootScope.user=$localStorage.user;
                if (response.status) {
                    $ionicPopup.alert({
                        title: "Email Confirmed Successfully",
                        template: response.message
                    }).then(function(res) {
                        console.log(response.data.status);
                   if (response.data.status == 1) {
                        $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                          $state.go("account_activation",null,{reload:true});
                    } else {
                         $ionicHistory.nextViewOptions({
                            disableBack: true
                        });
                        $state.go("app.user_record_one",null,{reload:true});
                    }
                    });
                } else {
                    $ionicPopup.alert({
                        title: "ERROR",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'OTP Confirmation Failed',
                    template: 'API is not working'
                });
            });
        }
    }
     $scope.resend_confirmOTP = function () {
        
        if ($scope.data.email === '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Email is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/resent_otp",
                method: 'POST',
                data: {
                    email: $scope.data.email
                }
            }).success(function (res) {
                console.log(res);
                 $ionicLoading.hide();
                 if (res.status) {      
                      $ionicPopup.alert({
                    title: 'Success',
                    template: res.message
                });
                 }else
                 {
                      $ionicPopup.alert({
                    title: 'Error',
                    template: res.message
                });
                 }
               
            }).error(function (error) {
                console.log(error);
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'Error',
                    template: 'API is not working'
                });
            });
        }
    };
});
    