angular.module('aura-genie.controllers').controller('pastRecordCtrl', function($scope,$location,$state, $localStorage,$http,$filter,$ionicPopup, $rootScope, $ionicLoading,$stateParams) {
    
    $ionicLoading.show();
    $http({
        url: $rootScope.apiUrl + "API/User/list_analysis",
        method: 'POST',
        data:  {
            record_id:$stateParams.record_id
        },
        headers: {
            'Access-Token': $localStorage.user.token,
            'Email': $localStorage.user.email
        }
    }).success(function(response) {
        $ionicLoading.hide();
        if (response.status) {
            $scope.recordList = response.data;
            for(var i=0;i<$scope.recordList.length;i++){
               $scope.recordList[i].date=$filter('date')($scope.recordList[i].date,'MMM dd,yyyy');
            }
        } else {
            $ionicPopup.alert({
                title: "Error",
                template: response.message
            });
        }
    }).error(function(error) {
        $ionicLoading.hide();
        $ionicPopup.alert({
            title: 'API/User/list_user_record',
            template: 'API is not working'
        });
    });
    
    
    
    
    $rootScope.showPastAnalysis=false;
    
    $scope.analysisDetail=function(analysis_id){
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User/analysis_detail",
            method: 'POST',
             data:    
             {
                 analysis_id:analysis_id
             },

            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
             $rootScope.resultScan=response.data.analysis;
             console.log(response.data.analysis);
             $state.go("app.system_overview",{user_record_id:$stateParams.user_record_id},{reload:true});
             $rootScope.showPastAnalysis=true;
        }).error(function(error) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'API/User/list_user_record',
                template: 'API is not working'
            });
        });
    };
    
});