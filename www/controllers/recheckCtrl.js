angular.module('aura-genie.controllers').controller('recheckCtrl', function($scope,$ionicPopup,$state) {
    $scope.scan=false;
    $scope.recheckPopup;
    $scope.scanProcess=function(){
        $scope.scan=true;
        $scope.recheckPopup=$ionicPopup.show({
                    templateUrl: "templates/form-popup/supportive_popup.html",
                    buttons:[],
                    cssClass:'recheck_style',
                    scope:$scope
            });
    }
    $scope.recheckList=[
        {
            id:0,name:'Angst',selected:false
        },
        {
            id:1,name:'Annoyance',selected:false
        },
        {
            id:2,name:'Anxiety',selected:false
        },
        {
            id:3,name:'Apathy',selected:false
        },
        {
            id:4,name:'Awe',selected:false
        },
        {
            id:5,name:'Boredom',selected:false
        }
    ];
    $scope.imPrinter=function()
    {
       $ionicPopup.alert({
                    template: "Make Sure Your Printer Is Connected With Device",
                    cssClass:'scan-complete-popup',
                    buttons:[{
                            text:"Ok"
                    },
                    {
                       text:"Cancel" 
                    }
                  ]
            });
    };
    $scope.hololinguistics=function()
    {
        
       $ionicPopup.alert({
                    template: "It Appears Your IMprinter Is Not Connected. To Start Hololinguistics Press OK",
                    cssClass:'scan-complete-popup',
                    style:'color:red',
                    buttons:[{
                            text:"Ok"
                    },
                    {
                       text:"Cancel",
//                       onTap:function()
//                       {
//                           $scope.recheckPopup.close();
//                       }
                    }
                  ]
            });
    };
});