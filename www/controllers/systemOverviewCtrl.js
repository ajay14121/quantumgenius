angular.module('aura-genie.controllers').controller('systemOverviewCtrl', function ($scope, $stateParams, $localStorage, $http, $rootScope, $state, $timeout, $interval, $ionicPopup, $cordovaMedia, $cordovaCapture) {
    $scope.IsVisible = false;
    $scope.hideScan = false;
    $scope.classFooter = 'bar-subfooter';
    $scope.progressCmplte = false;
    $scope.listCanSwipe=false;
    $scope.changeIconBRx = true;
    $scope.itemCountBRx = 0;
    $scope.itemCountMRx = 0;
    $scope.flag = false;
    $scope.playItemsBRx = [];
    $scope.playItemsMRx = [];
    $scope.frequencyBRx=[];
    $scope.frequencyMRx=[];
    $scope.changeIconMRx = true;
    $scope.subListName = '';
    $scope.itemsRemainBRx=0;
    $scope.itemsRemainMRx=0;
    $scope.displayMoreBRx=false;
    $scope.displayMoreMRx=false;
    $scope.durationBRx= 0; 
    $scope.durationMRx= 0; 
    $scope.data={};
    $scope.data.timeControlValueBRx=0;
    $scope.data.timeControlValueMRx=0;
    $scope.data.limitOfItemsBRx=2;
    $scope.data.limitOfItemsMRx=2;
    console.log($rootScope.showPastAnalysis);
        if($rootScope.showPastAnalysis==true)
        {
            $scope.hideScan=true;
            $scope.IsVisible=true;
            $scope.classFooter = 'bar-footer';
             for(var i=0;i<$rootScope.resultScan.length;i++)
             {
                 $rootScope.system_overview[i].randomValue=$rootScope.resultScan[i].randomValue;
                 $rootScope.system_overview[i].topClassActive=$rootScope.resultScan[i].topClassActive;
             }
        }
        else
        {
            $scope.hideScan=false;
            $scope.IsVisible=false;
            $scope.classFooter = 'bar-subfooter';
            
        }
        $scope.displayHelp=function(){
                        $ionicPopup.show({
                title: 'Help Section',
                template: '<div><p  class="text-left" style="height:200px;overflow-y:auto;overflow-x:hidden;margin:0px 0px 37px 0px;font-size:12px"> this is the help section,here you can find description about pages\n\
                         this is the help section,here you can find description about pages this is \n\
                        the help section,here you can find description about pages this is the help\n\
                        section,here you can find description about pagesthis is the help section,here \n\
                        you can find description about pages\n\\n\
                        this is the help section,here you can find description about pages\n\
                         this is the help section,here you can find description about pages this is \n\
                        the help section,here you can find description about pages this is the help\n\
                        section,here you can find description about pagesthis is the help section,here \n\
                        you can find description about pages\n\\n\
                         this is the help section,here you can find description about pages this is \n\
                        the help section,here you can find description about pages this is the help\n\
                        section,here you can find description about pagesthis is the help section,here \n\
                        you can find description about pages\n\
                         </p></div>',
                cssClass: 'help_popup',
                buttons:[
                    {
                        text:"ok",
                        onTap:function(){
                            console.log("function called");
                        }
                    }
                ]
            });
        };
    $scope.findParams = function (uri,title){
        console.log(uri);
        $rootScope.uriSelected = uri;
        $rootScope.titleFirst=title;
        if ($scope.progressCmplte == true)
        {
            $state.go("app.system_overview_two",{user_record_id:$stateParams.user_record_id});
        }
    };
    $scope.playTuneBRx = function () {
        console.log('Testing start');
        console.log($scope.data.timeControlValueBRx);
            console.log("totaL TIME"+$scope.data.timeControlValueBRx);
            $scope.durationBRx = (parseInt($scope.data.timeControlValueBRx)/$scope.itemCountBRx) * 60000;
            console.log($scope.durationBRx);
             console.log('Testing start');
        if($scope.data.timeControlValueBRx == 0){
            $ionicPopup.alert({
                    title: 'Oops',
                    template: 'Timer value should be greater than zero'
            });
            return false;
        }

        try {
            var media = new Media("/android_asset/www/beep.wav", function (e) {
                console.log("success");
            }, function (e) {
                console.log("error");
            });
            $scope.durationTimeout = 0;
            media.playTone($scope.durationBRx,$scope.frequencyBRx);

        } catch (e) {
            alert("exception" + e);
            console.log("Play Sound on Devices");
        }
    };
    $scope.showPlayerBRx = function ()
    {
        $scope.IsVisibleBRx = true;
        $scope.changeIconBRx = false;
        $scope.playerOpenClassBRx = 'has-subheader-player';
    };

    $scope.hidePlayerBRx = function ()
    {
        $scope.IsVisibleBRx = false;
        $scope.changeIconBRx = true;
        $scope.playerOpenClassBRx = '';
    };
    $scope.deleteItemBRx = function (index) {
        $scope.playItemsBRx.splice(index, 1);
        $scope.frequencyBRx.splice(index, 1);
        $scope.itemCountBRx--;
        
    };
    $scope.displayAllBRx=function(){
        $scope.data.limitOfItemsBRx=$scope.itemCountBRx;
        $scope.displayMoreBRx=false;
    }
    $scope.addBRx = function (data) {

        $scope.matches = true;

        if ($scope.itemCountBRx >=2) {
            $scope.data.limitOfItemsBRx = 2;
            $scope.displayMoreBRx=true;
        }
        angular.forEach($scope.playItemsBRx, function (item) {
            if (data.id === item.id) {
                $scope.matches = false;
            }
        });
          
        // add item to collection
        if ($scope.matches != false) {
            $scope.playItemsBRx.push(data);
            $scope.frequencyBRx.push(data.frequency);
            $scope.itemCountBRx = $scope.playItemsBRx.length;
            for (var i = 0; i < $rootScope.system_overview[data.uri].length; i++)
            {
               
                    $scope.frequencyBRx.push($rootScope.system_overview[data.uri][i].frequency);
                    $scope.subListName = $rootScope.system_overview[data.uri][i].uri;
                    console.log($rootScope.system_overview[data.uri][i]);
                    if( $scope.subListName !=  undefined ){
                        for (var j = 0; j < $rootScope.system_overview[data.uri][$scope.subListName].length; j++)
                        {
                            $scope.frequencyBRx.push($rootScope.system_overview[data.uri][$scope.subListName][j].frequency);
                        }
                    }
            }
         console.log($scope.itemCountBRx);
            if($scope.itemCountBRx>2){
                $scope.itemsRemainBRx=$scope.itemCountBRx-2;
                $scope.displayMoreBRx=true;
            }
        } 
        
       
    };
    $scope.showPlayerMRx = function ()
    {
        $scope.IsVisibleMRx = true;
        $scope.changeIconMRx = false;
        $scope.IsVisibleBRx = false;
//        $scope.playerOpenClassMRx = 'has-subfooter-player';
    };
    $scope.hidePlayerMRx = function ()
    {
        $scope.IsVisibleMRx = false;
        $scope.changeIconMRx = true;
//        $scope.playerOpenClassMRx = '';
    };
    $scope.deleteItemMRx = function (index) {
        $scope.playItemsMRx.splice(index, 1);
        $scope.frequencyMRx.splice(index, 1);
        $scope.itemCountMRx--;
    };
    $scope.addMRx = function (data) {
        console.log("drop method in mrx");
        $scope.matches = true;
        console.log("timer"+$scope.data.timeControlValueMRx);
        
        console.log($scope.itemCountMRx);
        if ($scope.itemCountMRx >=2) {
            $scope.data.limitOfItemsMRx = 2;
            $scope.displayMoreMRx=true;
        }
        angular.forEach($scope.playItemsMRx, function (item) {
            if (data.id === item.id) {
                $scope.matches = false;
            }
        });

        // add item to collection
        if ($scope.matches != false) {
            $scope.playItemsMRx.push(data);
            $scope.frequencyMRx.push(data.frequency);
            $scope.itemCountMRx = $scope.playItemsMRx.length;
            for (var i = 0; i < $rootScope.system_overview[data.uri].length; i++)
            {
               
                    $scope.frequencyMRx.push($rootScope.system_overview[data.uri][i].frequency);
                    $scope.subListName = $rootScope.system_overview[data.uri][i].uri;
                    if( $scope.subListName !=  undefined ){
                        for (var j = 0; j < $rootScope.system_overview[data.uri][$scope.subListName].length; j++)
                        {

                            $scope.frequencyMRx.push($rootScope.system_overview[data.uri][$scope.subListName][j].frequency);

                        }
                    }
            }
        }
        if($scope.itemCountMRx>2){
            $scope.itemsRemainMRx=$scope.itemCountMRx-2;
            $scope.displayMoreMRx=true;
        }

        $scope.itemCountMRx = $scope.playItemsMRx.length;
    };
    $scope.displayAllMRx=function(){
        $scope.data.limitOfItemsMRx=$scope.itemCountMRx;
        $scope.displayMoreMRx=false;
    }
    $scope.playTuneMRx = function () {
        console.log('Testing start');
        
            console.log("totaL TIME"+$scope.data.timeControlValueMRx);
            $scope.durationMRx = (parseInt($scope.data.timeControlValueMRx)/$scope.itemCountMRx) * 60000;
            console.log("duration"+$scope.durationMRx);
        if($scope.data.timeControlValueMRx == 0){
            $ionicPopup.alert({
                    title: 'Oops',
                    template: 'Timer value should be greater than zero'
            });
            return false;
        }

        try {
            var media = new Media("/android_asset/www/beep.wav", function (e) {
                console.log("success");
            }, function (e) {
                console.log("error");
            });
            $scope.durationTimeout = 0;
//            for(var i=0;i<$scope.frequencyMRx.length;i++){
//                console.log("duration"+$scope.duration);
//                $timeout(function(){media.playTone($scope.durationMRx,$scope.frequencyMRx[i])},$scope.durationTimeout);
//                $scope.durationTimeout = $scope.durationTimeout + $scope.duration;
//            }
            media.playTone($scope.durationMRx,$scope.frequencyMRx);
        } catch (e) {
            alert("exception" + e);
            console.log("Play Sound on Devices");
        }
    };
    $scope.getRandomValue = function () {
        $scope.listCanSwipe=false;
        $scope.hideScan = true;
        $scope.classFooter = 'bar-footer';
        $scope.timeoutLimit=((Math.random()*777)%10+16)*1000;
        console.log($scope.timeoutLimit);
        $scope.intervalRef = $interval(function () {
            for (var i = 0; i < 5; i++) {

                $rootScope.system_overview[i].randomValue = parseInt(Math.random() * 777);
            }
            for (var index = 0; index < $rootScope.system_overview.length; index++) {
        if ($rootScope.system_overview[index].randomValue >= 0 && $rootScope.system_overview[index].randomValue <= 150) {
            $rootScope.system_overview[index].topClassActive = 'activeBlue';
            console.log("blue");
        }
        if($rootScope.system_overview[index].randomValue >= 151 && $rootScope.system_overview[index].randomValue <= 450) {
            $rootScope.system_overview[index].topClassActive = 'activeGreen';
            console.log("green");
        }
        if($rootScope.system_overview[index].randomValue >= 451 && $rootScope.system_overview[index].randomValue <= 600) {
            $rootScope.system_overview[index].topClassActive = 'activeYellow';
            console.log("yellow");
        }
        if($rootScope.system_overview[index].randomValue >= 601 && $rootScope.system_overview[index].randomValue <= 777) {
            $rootScope.system_overview[index].topClassActive = 'activeRed';
            console.log("red");
        }
            }
        
        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
	    
        }, $scope.timeoutLimit)
                .then(function () {
            console.log($rootScope.system_overview);
	    $scope.progressCmplte = true;
            $scope.listCanSwipe=true;
//            $ionicPopup.show({
//                title: 'Help Section',
//                template: '<p style="overflow:scroll;max-height:200px"> this is the help section,here you can find description about pages\n\
//                         this is the help section,here you can find description about pages this is \n\
//                        the help section,here you can find description about pages this is the help\n\
//                        section,here you can find description about pagesthis is the help section,here \n\
//                        you can find description about pages\n\
//                         </p>',
//                cssClass: 'help_popup'
//            });
            $http({
                url: $rootScope.apiUrl + "API/User/add_analysis_record",
                method: 'post',
                data: {
                    record_id: $stateParams.user_record_id,
                    result: $rootScope.system_overview
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email,
                }
            }).success(function (response)
            {
                console.log("success" + response);
            }).error(function (error) {
                console.log(error);
            })

        });


        $scope.IsVisible = $scope.IsVisible ? false : true;

    };
//     $scope.descriptionMind=function() {
//          for(var i=0;i<6;i++){
//            $rootScope.randomMindValue[i]=parseInt(Math.random()*777);
//        }
//       $state.go("app.system_overview_three");
//     };
});