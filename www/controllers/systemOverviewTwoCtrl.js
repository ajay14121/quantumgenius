angular.module('aura-genie.controllers').controller('systemOverviewTwoCtrl', function ($scope,$interval,$http,$localStorage,$stateParams,$rootScope,$ionicPopup,$interval, $rootScope, $timeout, $cordovaMedia, $cordovaCapture,$state) {
    $scope.IsVisible = false;
    $scope.progressCmplte = false;
    $scope.playItemsBRx = [];
    $scope.playItemsMRx = [];
    $scope.listCanSwipe=false;
    $scope.itemCountBRx = 0;
    $scope.itemCountMRx = 0;
    $scope.classFooter = 'bar-subfooter';
    $scope.IsVisible = false;
    $scope.changeIconBRx = true;
    $scope.changeIconMRx = true;
    $scope.IsVisibleMRx = false;
    $scope.random = [];
    $scope.frequencyBRx=[];
    $scope.frequencyMRx=[];
    $scope.durationBRx=0;
    $scope.itemsRemainBRx=0;
    $scope.displayMoreBRx=false;
    $scope.durationMRx=0;
    $scope.data={};
    $scope.data.limitOfItemsBRx=2;
    $scope.displayMoreMRx=false;
    $scope.data.timeControlValueBRx=0;
    $scope.data.timeControlValueMRx=0;
    $scope.data.limitOfItemsMRx=2;
    $scope.paramGroup = $rootScope.system_overview[$rootScope.uriSelected];
////    $scope.title = $rootScope.system_overview[$rootScope.index].title;
//    console.log($scope.title);
    $scope.displayHelp = function () {
        $ionicPopup.show({
            title: 'Help Section',
            template: '<div><p style="height:200px;overflow-y:auto;overflow-x:hidden;margin:0px 0px 37px 0px"> this is the help section,here you can find description about pages\n\
                         this is the help section,here you can find description about pages this is \n\
                        the help section,here you can find description about pages this is the help\n\
                        section,here you can find description about pagesthis is the help section,here \n\
                        you can find description about pages\n\\n\
                        this is the help section,here you can find description about pages\n\
                         this is the help section,here you can find description about pages this is \n\
                        the help section,here you can find description about pages this is the help\n\
                        section,here you can find description about pagesthis is the help section,here \n\
                        you can find description about pages\n\\n\
                         this is the help section,here you can find description about pages this is \n\
                        the help section,here you can find description about pages this is the help\n\
                        section,here you can find description about pagesthis is the help section,here \n\
                        you can find description about pages\n\
                         </p></div>',
            cssClass: 'help_popup',
            buttons: [
                {
                    text: "ok",
                    onTap: function () {
                        console.log("function called");
                    }
                }
            ]
        });
    };
    $scope.playTuneBRx = function () {
        console.log('Testing start');
        console.log($scope.data.timeControlValueBRx);
       
         
            console.log("totaL TIME"+$scope.data.timeControlValueBRx);
            $scope.durationBRx = (parseInt($scope.data.timeControlValueBRx)/$scope.itemCountBRx) * 60000;
            console.log($scope.durationBRx);
             console.log('Testing start');
        if($scope.data.timeControlValueBRx == 0){
            $ionicPopup.alert({
                    title: 'Oops',
                    template: 'Timer value should be greater than zero'
            });
            return false;
        }

        try {
            var media = new Media("/android_asset/www/beep.wav", function (e) {
                console.log("success");
            }, function (e) {
                console.log("error");
            });
            $scope.durationTimeout = 0;
//            for(var i=0;i<$scope.frequencyBRx.length;i++){
//                console.log("duration"+$scope.duration);
//                $timeout(function(){media.playTone($scope.durationBRx,$scope.frequencyBRx[i])},$scope.durationTimeout);
//                $scope.durationTimeout = $scope.durationTimeout + $scope.duration;
//            }
//            console.log($scope.frequencyBRx);
            media.playTone($scope.durationBRx,$scope.frequencyBRx);
        } catch (e) {
            alert("exception" + e);
            console.log("Play Sound on Devices");
        }
    };
   

    $scope.findParams = function (uri,title)
    {
        $rootScope.paramList = uri;
        $rootScope.title = title;
       if ($scope.progressCmplte == true)
        {
            $state.go("app.system_overview_three",{user_record_id:$stateParams.user_record_id});
        }
    };
    $scope.showPlayerBRx = function ()
    {
        $scope.IsVisibleBRx = true;
        $scope.changeIconBRx = false;
        $scope.playerOpenClassBRx = 'has-subheader-player';
    };

    $scope.hidePlayerBRx = function ()
    {
        $scope.IsVisibleBRx = false;
        $scope.changeIconBRx = true;
        $scope.playerOpenClassBRx = 'has-subheader-player';
    };
    $scope.deleteItemBRx = function (index) {
        console.log(index);
        $scope.playItemsBRx.splice(index, 1);
        $scope.frequencyBRx.splice(index, 1);
    };
      $scope.displayAllBRx=function(){
        $scope.data.limitOfItemsBRx=$scope.itemCountBRx;
        $scope.displayMoreBRx=false;
    };
   
    $scope.addBRx = function (data) {
        $scope.matches = true;
        if($scope.itemCountBRx >2){
            $scope.data.limitOfItemsBRx = 2;
            $scope.displayMoreBRx=true;
        }
        angular.forEach($scope.playItemsBRx, function (item) {
            if (data.id === item.id) {
                $scope.matches = false;
            }
        });

        // add item to collection
        if ($scope.matches != false) {
            $scope.playItemsBRx.push(data);
            $scope.frequencyBRx.push(data.frequency);
            $scope.itemCountBRx = $scope.playItemsBRx.length;
            if( data.uri !=  undefined ){
                for(var i=0;i<$scope.paramGroup[data.uri].length;i++)
                {
                   $scope.frequencyBRx.push($scope.paramGroup[data.uri][i].frequency); 
                }
            }
            for (var i = 0;i<$scope.frequencyBRx.length;i++)
            {
                console.log($scope.frequencyBRx[i]);
            }
            
        }
          if($scope.itemCountBRx>2){
            $scope.itemsRemainBRx=$scope.itemCountBRx-2;
            $scope.displayMoreBRx=true;
        }
        $scope.itemCountBRx = $scope.playItemsBRx.length;
    };
     $scope.playTuneMRx = function () {
        console.log($scope.playItemsMRx.length);
        console.log('Testing start');
       
            console.log("totaL TIME"+$scope.data.timeControlValueMRx);
            $scope.durationMRx = (parseInt($scope.data.timeControlValueMRx)/$scope.itemCountMRx) * 60000;
            console.log("duration"+$scope.durationMRx);
        if($scope.data.timeControlValueMRx == 0){
            $ionicPopup.alert({
                    title: 'Oops',
                    template: 'Timer value should be greater than zero'
            });
            return false;
        }

        try {
            var media = new Media("/android_asset/www/beep.wav", function (e) {
                console.log("success");
            }, function (e) {
                console.log("error");
            });
            $scope.durationTimeout = 0;
//            for(var i=0;i<$scope.frequencyMRx.length;i++){
//                console.log("duration"+$scope.duration);
//                $timeout(function(){media.playTone($scope.durationMRx,$scope.frequencyMRx[i])},$scope.durationTimeout);
//                $scope.durationTimeout = $scope.durationTimeout + $scope.duration;
//            }
//            console.log($scope.frequencyMRx);
          media.playTone($scope.durationMRx,$scope.frequencyMRx);
        } catch (e) {
            alert("exception" + e);
            console.log("Play Sound on Devices");
        }
    };
    $scope.showPlayerMRx = function ()
    {
        $scope.IsVisibleMRx = true;
        $scope.changeIconMRx = false;
    };
    $scope.hidePlayerMRx = function ()
    {
        $scope.IsVisibleMRx = false;
        $scope.changeIconMRx = true;
    };
    $scope.deleteItemMRx = function (index) {
        $scope.playItemsMRx.splice(index, 1);
        $scope.frequencyMRx.splice(index, 1);
    };
     $scope.displayAllMRx=function(){
        $scope.data.limitOfItemsMRx=$scope.itemCountMRx;
        $scope.displayMoreMRx=false;
    }
    $scope.addMRx = function (data) {
        $scope.matches = true;
        
         if($scope.itemCountMRx >2){
            $scope.data.limitOfItemsMRx = 2;
            $scope.displayMoreMRx=true;
        }
        angular.forEach($scope.playItemsMRx, function (item) {
            if (data.id === item.id) {
                $scope.matches = false;
            }
        });

        // add item to collection
        if ($scope.matches != false) {
            $scope.playItemsMRx.push(data);
            $scope.frequencyMRx.push(data.frequency);
             $scope.itemCountMRx = $scope.playItemsMRx.length;
            if( data.uri !=  undefined ){
            for(var i=0;i<$scope.paramGroup[data.uri].length;i++)
                {
                   $scope.frequencyMRx.push($scope.paramGroup[data.uri][i].frequency); 
                }
            }
            for (var i = 0;i<$scope.frequencyMRx.length;i++)
            {
                console.log($scope.frequencyMRx[i]);
        }
        }
        if($scope.itemCountMRx> 2){
            $scope.itemsRemainMRx=$scope.itemCountMRx-2;
            $scope.displayMoreMRx=true;
        }

        $scope.itemCountMRx = $scope.playItemsMRx.length;
    };
    $scope.getRandomValue = function () {
$scope.listCanSwipe=false;
        $scope.hideScan = true;
        $scope.classFooter = 'bar-footer';
        $scope.timeoutLimit = ((Math.random() * 777) % 10 + 16) * 1000;
        console.log($scope.timeoutLimit);
        $scope.intervalRef = $interval(function () {
       for (var i = 0; i < $scope.paramGroup.length; i++)
    {
         $scope.paramGroup[i].randomValue = parseInt(Math.random() * 777) ;
    }
    for (var index = 0; index < $scope.paramGroup.length; index++) {
        if ($scope.paramGroup[index].randomValue >= 0 && $scope.paramGroup[index].randomValue <= 150) {
            $scope.paramGroup[index].topClassActive = 'activeBlue';
        }
        if($scope.paramGroup[index].randomValue >= 151 && $scope.paramGroup[index].randomValue <= 450) {
            $scope.paramGroup[index].topClassActive = 'activeGreen';
        }
        if($scope.paramGroup[index].randomValue >= 451 && $scope.paramGroup[index].randomValue <= 600) {
            $scope.paramGroup[index].topClassActive = 'activeYellow';
        }
        if($scope.paramGroup[index].randomValue >= 601 && $scope.paramGroup[index].randomValue <= 777) {
            $scope.paramGroup[index].topClassActive = 'activeRed';
        }
    }

        }, 100);
        var myTimer = $timeout(function () {
            $interval.cancel($scope.intervalRef);
            for (var index = 0; index < $rootScope.system_overview.mind.length; index++) {
                $rootScope.system_overview.mind[index].randomValue = parseInt(Math.random() * 777);
            }
             for (var index = 0; index < $rootScope.system_overview.body.length; index++) {
                $rootScope.system_overview.body[index].randomValue = parseInt(Math.random() * 777);
            }
             for (var index = 0; index < $rootScope.system_overview.energy.length; index++) {
                $rootScope.system_overview.energy[index].randomValue = parseInt(Math.random() * 777);
            }

            for (var index = 0; index < $rootScope.system_overview.spirituality.length; index++) {
                $rootScope.system_overview.spirituality[index].randomValue = parseInt(Math.random() * 777);
            }

            for (var index = 0; index < $rootScope.system_overview.libraries.length; index++) {
                $rootScope.system_overview.libraries[index].randomValue = parseInt(Math.random() * 777);
            }
             for (var index = 0; index < $scope.paramGroup.length; index++) {
        if ($scope.paramGroup[index].randomValue >= 0 && $scope.paramGroup[index].randomValue <= 150) {
            $scope.paramGroup[index].topClassActive = 'activeBlue';
        }
        if($scope.paramGroup[index].randomValue >= 151 && $scope.paramGroup[index].randomValue <= 450) {
            $scope.paramGroup[index].topClassActive = 'activeGreen';
        }
        if($scope.paramGroup[index].randomValue >= 451 && $scope.paramGroup[index].randomValue <= 600) {
            $scope.paramGroup[index].topClassActive = 'activeYellow';
        }
        if($scope.paramGroup[index].randomValue >= 601 && $scope.paramGroup[index].randomValue <= 777) {
           $scope.paramGroup[index].topClassActive = 'activeRed';
        }
    }
     }, $scope.timeoutLimit).then(function(){
         console.log($stateParams.user_record_id);
$scope.progressCmplte = true;
            $scope.listCanSwipe=true;
                     $http({
                url: $rootScope.apiUrl + "API/User/add_analysis_record",
                method: 'post',
                data: {
                    record_id: $stateParams.user_record_id,
                    result: $scope.paramGroup
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email,
                }
            }).success(function (response)
            {
                console.log("success" + response);
            }).error(function (error) {
                console.log(error);
            })
            console.log($rootScope.system_overview.mind);
     });
      $scope.IsVisible = $scope.IsVisible ? false : true;
     };
});
