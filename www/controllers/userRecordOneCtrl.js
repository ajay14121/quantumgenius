angular.module('aura-genie.controllers').controller('userRecordOneCtrl', function($filter,$scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage, $state, $stateParams) {
    $scope.data = {};
    $scope.data.first_name = '';
    $scope.data.last_name = '';
    $scope.data.dob = '';
     $scope.user_record_id=0;
    // Date of Birth Call Back functions
    $scope.datePickerCallback = function(val) {
        if (typeof (val) === 'undefined') {
            console.log('Date not selected.');
        } else {
            $scope.data.dob = val.getFullYear() + '-' + (val.getMonth() + 1) + '-' + val.getDate();
            var suffixes = ["th", "st", "nd", "rd"];
            var day = $filter('date')(val, 'd');
            var relevantDigits = (day < 30) ? day % 20 : day % 30;
            var suffix = (relevantDigits <= 3) ? suffixes[relevantDigits] : suffixes[0];
            $scope.data.dob_show_format = day+suffix+$filter('date')(val,' MMM, yyyy');
            console.log('Selected date is : ', val);
        }
    };

    $scope.sendDetailOne = function() {
        if ($scope.data.first_name == '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'First name is mandatory'
            });
        } else if (!$rootScope.name.test($scope.data.first_name)) {
            $ionicPopup.alert({
                title: 'Invalid First Name',
                template: 'Only characters are allowed'
            });
        }else if ($scope.data.last_name == '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Last name is mandatory'
            });
        } else if (!$rootScope.name.test($scope.data.last_name)) {
            $ionicPopup.alert({
                title: 'Invalid Last Name',
                template: 'Only characters are allowed'
            });
        }else if ($scope.data.dob == '') {
            $ionicPopup.alert({
                title: 'Empty Field',
                template: 'Date of birth is mandatory'
            });
        } else {
            $ionicLoading.show();
            $http({
                url: $rootScope.apiUrl + "API/User/add_user_record",
                method: 'POST',
                data: {
                    first_name: $scope.data.first_name,
                    last_name: $scope.data.last_name,
                    dob: $scope.data.dob
                },
                headers: {
                    'Access-Token': $localStorage.user.token,
                    'Email': $localStorage.user.email
                }
            }).success(function(response) {
                $ionicLoading.hide();
                if (response.status) {
                    $scope.user_record_id=response.data.record_id;
                    $ionicPopup.alert({
                        title: "Client Added",
                        template: response.message
                    }).then(function(alert_response) {
                        $state.go("app.user_record_two", {user_record_id: response.data.record_id});
                    });
                } else {
                    $ionicPopup.alert({
                        title: "Failed",
                        template: response.message
                    });
                }
            }).error(function(erorr) {
                $ionicLoading.hide();
                $ionicPopup.alert({
                    title: 'API error',
                    template: 'API is not working'
                });
            });
        }
    };
});
    