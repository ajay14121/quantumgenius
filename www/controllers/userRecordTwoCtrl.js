angular.module('aura-genie.controllers').controller('userRecordTwoCtrl', function($scope, $http, $location, $ionicPopup, $rootScope, $ionicLoading, $stateParams, $localStorage,$state) {
    $scope.data = {};
    $scope.data.symptoms_detail = '';   
    $scope.symptoms = [
        {id: 1, symptom_name: 'Allergies', activeClass:'', selected:false },
        {id: 2, symptom_name: 'Anxiety', activeClass:'', selected:false },
        {id: 3, symptom_name: 'Arthritis', activeClass:'', selected:false },
        {id: 4, symptom_name: 'Asthama', activeClass:'', selected:false },
        {id: 5, symptom_name: 'Bad Circulation', activeClass:'', selected:false },
        {id: 6, symptom_name: 'Bruise Easily', activeClass:'', selected:false },
        {id: 7, symptom_name: 'CardioVascular', activeClass:'',selected:false },
        {id: 8, symptom_name: 'Digertive Disease', activeClass:'',selected:false },
        {id: 9, symptom_name: 'Depression', activeClass:'',selected:false},
        {id: 10, symptom_name: 'Diabetes', activeClass:'', selected:false},
        {id: 11, symptom_name: 'Head Aches', activeClass:'', selected:false},
        {id: 12, symptom_name: 'High Blood Pressure', activeClass:'', selected:false},
        {id: 13, symptom_name: 'Painful Joints', activeClass:'', selected:false}
    ];
    $scope.suffer_from = {
        selections: [
        ]
    };
    
    $scope.bgChange = function(index,id,value){ 
        console.log("index: " + index + "id: " + id + "value : " +value)
        if($scope.symptoms[index].activeClass){
            $scope.symptoms[index].activeClass= "";
            $scope.symptoms[index].selected= false;
            for(var i=0;i<$scope.suffer_from.selections.length;i++)
            {
                if($scope.suffer_from.selections[i].id==id)
                {
                    $scope.removeItem=i;
                }
                
            }
            $scope.suffer_from.selections.splice($scope.removeItem,1);
            console.log($scope.suffer_from.selections);
        } else {
            console.log("add");
            $scope.symptoms[index].activeClass= "active";
            $scope.symptoms[index].selected= true;
            var object={};
            object.id=id;
            object.value=value;
            console.log(object);
           $scope.suffer_from.selections.push(object);
           console.log($scope.suffer_from.selections);
        }          
    };
    $scope.beginVoiceAnalysis = function() {
         console.log($scope.suffer_from.selections);
          if($scope.data.symptoms_detail=='')
          {
                $ionicPopup.alert({
                    title: "Wellness Markers",
                    template: "Please be sure to add your client's current symptoms."
                });
          } else {
        $ionicLoading.show();
        $http({
            url: $rootScope.apiUrl + "API/User/update_user_record",
            method: 'POST',
            data: {
                first_name: $stateParams.user_first_name,
                last_name: $stateParams.user_last_name,
                dob: $stateParams.user_dob,
                suffer_from: $scope.suffer_from.selections,
                symptoms_detail: $scope.data.symptoms_detail,
                record_id: $stateParams.user_record_id
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email
            }
        }).success(function(response) {
            $ionicLoading.hide();
            if (response.status) {
                $ionicPopup.alert({
                    title: "Wellness Markers Added",
                    template: "You can change your client's wellness markers with each and every visit."
                }).then(function(res) {
                    //$location.path("/app/voice_analysis");
                    $state.go("app.voice_analysis", {user_record_id: $stateParams.user_record_id});
                });
            } else {
                $ionicPopup.alert({
                    title: "authentication failed",
                    template: response.message
                });
            }
        }).error(function(erorr) {
            $ionicLoading.hide();
            $ionicPopup.alert({
                title: 'failed',
                template: 'API is not working'
            });
        });
    };
    };
});
    