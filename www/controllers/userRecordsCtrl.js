angular.module('aura-genie.controllers').controller('userRecordsCtrl', function($scope, $state,$http, $location, $ionicPopup, $rootScope, $ionicLoading, $localStorage) {
    $scope.data = {};
    $scope.data.users = '';
    // dynamically increasing or decreasing the text limit according to window size
     $scope.$watch('window.innerWidth', function() {
        $scope.numLimit = window.innerWidth/4;
     });
     $scope.pastRecords=function(id)
     {

         
     };
      $scope.readMore=function(index){
          console.log($scope.data.users[index]);
                  $scope.data.users[index].numLimit =100000;
                  $scope.data.users[index].hide_more=false;  
      };
    $ionicLoading.show();
    $http({
        url: $rootScope.apiUrl + "API/User/list_user_record",
        method: 'POST',
        headers: {
            'Access-Token': $localStorage.user.token,
            'Email': $localStorage.user.email
        }
    }).success(function(response) {
        $ionicLoading.hide();
        if (response.status) {
            $scope.data.users = response.data;
        } else {
            $ionicPopup.alert({
                title: "Error",
                template: response.message
            });
        }
    }).error(function(erorr) {
        $ionicLoading.hide();
        $ionicPopup.alert({
            title: 'API/User/list_user_record',
            template: 'API is not working'
        });
    });
});
    