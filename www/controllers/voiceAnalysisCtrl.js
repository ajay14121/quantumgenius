angular.module('aura-genie.controllers').controller('voiceAnalysisCtrl', function($scope, $location, $interval, $rootScope, $cordovaMedia, $cordovaCapture, $stateParams, $state, $localStorage, $cordovaFileTransfer, $ionicLoading, $ionicPopup,$http,$timeout) {
    $scope.recordedAudioFile = '';
    $scope.recordingDuration = 30;
    $scope.recordingTimerPromise = '';
    $scope.time = 0;
    $scope.play=true;
    $scope.startScan=false;
    $scope.media='';
    //$scope.textTime = "00";
    
    $scope.playBtnDis=true;
    $scope.isPlay = true;
    $scope.recButtonImg = "img/record-w.png";
    $scope.recordClassBlack = "";
    $scope.playButtonImg = "img/play-w.png";
    $scope.textTime = function() {
        var output = $scope.time.toString()+ "";
        if($scope.time < 10) {
            output = '0' + $scope.time;
        }
        console.log(output);
        return output;
    };
    
    $scope.timer = function() {
        $scope.time++;
        if ($scope.time == $scope.recordingDuration) {
            $scope.isPlay=true;
            $scope.recButtonImg = "img/record-w.png";
            $scope.recordClassBlack = '';
            $scope.play=true;
            $scope.playBtnDis=false;
            $scope.playButtonImg = 'img/play-w.png';
            $interval.cancel($scope.recordingTimerPromise);
        }
    };
    $scope.startRecord = function() {
        if(!$scope.isPlay){
            return false;
        }
        $scope.stop();
        $scope.isPlay=false;
        $rootScope.recordedAudioFile= undefined;
        $scope.audioTemp=undefined;
        $scope.playBtnDis=true;
        $scope.recButtonImg = "img/mice-b.png";
        $scope.recordClassBlack = "black";
        var options = {limit: 1, duration: $scope.recordingDuration};
        try {
            $cordovaCapture.captureAudio(options).then(function(audioData) {
                // Success! Audio data is here
                console.log("Recording Start Success");
                var i, path, len;
                for (i = 0, len = audioData.length; i < len; i += 1) {
                    $rootScope.recordedAudioFile = audioData[i].fullPath;
                }
                $scope.uploadVoice();
                console.log(audioData);
            }, function(err) {
                // An error occurred. Show a message to the user
                console.log("Recording Start Error");
            });
        } catch (e) {
            console.log("Recorder will work on Devices only");
        }
        $scope.time = 0;
        $interval.cancel($scope.recordingTimerPromise);
        $scope.recordingTimerPromise = $interval($scope.timer, 1000);
        $ionicLoading.hide();
        $scope.isRecord=true;
    };
    
    $scope.playPauseRecord = function() {
        console.log("Play Audio");
        
        console.log($rootScope.recordedAudioFile);
         if($rootScope.recordedAudioFile == undefined)
        {
            var alertPopup = $ionicPopup.alert({
                            title: 'Error',
                            template: 'Record your voice'
                        });
        } else {
            try {
                if($scope.play){
                    $scope.time = 0;
                    $interval.cancel($scope.recordingTimerPromise);
                    $scope.recordingTimerPromise = $interval($scope.timer, 1000);
                    $scope.play=false;
                    $scope.playButtonImg = 'img/stop-icon.png';
                    console.log("play method");
                    $scope.media = $cordovaMedia.newMedia($rootScope.recordedAudioFile);
                    $scope.media.play();
                } else {
                    $scope.stop();
                }
            } catch (e) {
                console.log("Play Sound on Devices");
            }
        }
    };
    
    $scope.stop = function(){
        $scope.time = 0;
        $scope.play=true;
        $scope.playButtonImg = 'img/play-w.png';
        $interval.cancel($scope.recordingTimerPromise);
        try{
            $scope.media.stop();
        } catch(e){
             console.log("Stop Sound on Devices");
        }
    };
    
    $scope.remoteVoiceUpload = function() {
        //alert($rootScope.recordedAudioFile);
        if($rootScope.recordedAudioFile==undefined)
        {
            $ionicPopup.alert({
                            title: 'Wait',
                            template: 'Before we begin, please record your voice'
                        });
        } else {
        var server = $rootScope.apiUrl + "API/User/upload_record_file";
        //var filePath = cordova.file.documentsDirectory + "testImage.png";
        var filePath = $rootScope.recordedAudioFile;
        var trustHosts = true;
        var options = {
            params: {
                record_id: $stateParams.user_record_id,
                type: 'audio',
                file:$rootScope.recordedAudioFile
            },
            headers: {
                'Access-Token': $localStorage.user.token,
                'Email': $localStorage.user.email,
            },
            mimeType: 'audio/wav',
            fileName:'audio.wav',
        };
        try {
            $ionicLoading.show();
            $cordovaFileTransfer.upload(server, filePath, options)
                    .then(function(result) {
                        result = angular.fromJson(result);
                        response = angular.fromJson(result.response);

                        console.log('file upload success');
                        console.log(response);
                        
                        if (response.status) {
                            $scope.audioTemp = response.data.filename;
                            $ionicLoading.hide();
                            var alertPopup = $ionicPopup.alert({
                                title: 'Success',
                                template: 'Remote Voice Completed'
                            });
                            $scope.startScan=true;
                        } else {
                            $ionicLoading.hide();
                            $ionicPopup.alert({
                                title: 'Upload Audio Failed Temporary',
                                template: response.message
                            });
                        }
                        // Success!
                    }, function(err) {
                        $ionicLoading.hide();
                        console.log('Audio upload error');
                        console.log(err);
                        var alertPopup = $ionicPopup.alert({
                            title: 'API Error',
                            template: 'API is not working'
                        });
                        // Error
                    }, function(progress) {
                        //$ionicLoading.hide();
                        console.log(progress);
                        // constant progress updates
                    });

        } catch (e) {
            console.log('File Transfer Plugin will work on device');
            console.log(e);
        }
    }
    };
   //upload file on server
//    $scope.remoteVoiceUpload = function() {
//        if($rootScope.recordedAudioFile== '')
//        {
//            var alertPopup = $ionicPopup.alert({
//                            title: 'Wait',
//                            template: 'Before we begin, please record your voice'
//                        });
//        }
//        else{
//
//        try{           
//        //console.log($scope.audioTemp);
//        $ionicLoading.show();
//        $http({
//            url: $rootScope.apiUrl + "API/User/upload_record_file",
//            method: 'POST',
//            data: {
//                record_id: $stateParams.user_record_id,
//                type:'audio',
//                file: $rootScope.recordedAudioFile
//            },
//            headers: {
//                'Access-Token': $localStorage.user.token,
//                'Email': $localStorage.user.email
//            }
//        }).success(function(response) {
//            $ionicLoading.hide();
//            console.log('Record Updated');
//            console.log(response);
//            alert(response.status);
//            if (response.status) {
//                $ionicPopup.alert({
//                    title: "Success",
//                    template: "Remote Voice Completed"
//                });
//            } else {
//                $ionicPopup.alert({
//                    title: "Error",
//                    template: response.message
//                });
//            }
//        }).error(function(erorr) {
//            console.log('Record Updated');
//            console.log(erorr);
//            $ionicLoading.hide();
//            $ionicPopup.alert({
//                title: 'failed',
//                template: 'API is not working'
//            });
//        });
//        }catch(e){
//            console.log('File Transfer Plugin will work on device');
//            consoel.log(e);
//        }
//                    
//        }
//    };
    
    $scope.startAnalysis = function(){
        if($rootScope.recordedAudioFile== undefined )
        {
            var alertPopup = $ionicPopup.alert({
                            title: 'Wait',
                            template: 'Before we begin, please record your voice'
                        });
                       
        } else if($scope.startScan==false)
        {
            $ionicPopup.alert({
                            title: 'Wait',
                            template: 'Please Upload Voice First'
                        });
        }
        else{
//        
            $scope.popup = $ionicPopup.show({
            template:'<div class="text-center" style="width:60%;margin:0 auto"><ion-spinner class="bubbles"></ion-spinner><p>Scanning voice.....</p></div>',
            delay:'10000',
            cssClass:'scan-popup'
        });
        
       
        $timeout(function() {
            $scope.popup.close();
            $ionicPopup.show({
                template:'<p class="text-center">Voice analysis scan complete.</p>',
                cssClass:'scan-complete-popup',
                buttons:[{
                        text:'Continue',
                        onTap:function(){
                          $state.go("app.image_analysis",{user_record_id:$stateParams.user_record_id});  
                        }
                },
                {
                    text:'Redo My Voice Recording',
                    onTap:function(){
                        $scope.time = 0;
                        $state.go("app.voice_analysis",{user_record_id:$stateParams.user_record_id});
                    }
                }]
            });
        }, 3000);

        }  
    };
});    
