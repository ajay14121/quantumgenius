// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('aura-genie', ['ionic', 'aura-genie.controllers', 'ionic-datepicker', 'ngCordova', 'ngStorage', 'ngDraggable'])
        .run(function ($ionicPlatform, $rootScope, $localStorage, $cordovaDevice) {
            $ionicPlatform.ready(function () {
//                console.log("app.js");
//                 var src = "beep.wav";
 //                  var my_media = new Media("/android_asset/www/beep.wav", function(e){ console.log('sound playing')}, function(e){});
 //                  my_media.setRightVolume(90);
//                 my_media.setLeftVolume(90);
//                 my_media.getRightVolume();
//                 my_media.getLeftVolume();
//                 console.log("app.js");
                
                //$rootScope.apiUrl = "http://10.10.10.82:81/projects/aura_genie/";
                $rootScope.apiUrl = "http://giftsoninternet.com/all/aura_genie/";
                $rootScope.randomValue = [];
                $rootScope.paramList = '';
                $rootScope.showPastAnalysis=false;
                $rootScope.system_overview = [
                    {
                        id: 0, title: "Mind",
                        topClassActive: "",
                        uri: "mind", 
                        src: 'img/mind-img.png',
                        randomValue: '',
                        frequency: '655.73',
                        mind: [
                        ]
                    },
                    {
                        id: 1,
                        title: "Body",
                        topClassActive: "",
                        uri: "body", 
                        src: 'img/body-img.png',
                        randomValue: '',
                        frequency: '233.73',
                        body: [
                        ]

                    },
                    {
                        id: 2,
                        title: "Energy Field",
                        topClassActive: "",
                        uri: "energy",
                        src: 'img/energy-field-img.png',
                        randomValue: '',
                        frequency: '325.73',
                        energy: [
                        ]
                    },
                    {
                        id: 3,
                        title: "Spirituality",
                        topClassActive: "",
                        uri: "spirituality", 
                        src: 'img/etheric-img.png',
                        randomValue: '',
                        frequency: '345.73',
                        spirituality: [
                        ]
                    },
                    {
                        id: 4,
                        title: "Libraries",
                        topClassActive: "",
                        uri: "libraries", 
                        src: 'img/library.png',
                        randomValue: '',
                        frequency: '425.73',
                        libraries: [
                        ]
                    }
                ];
                $rootScope.system_overview.spirituality = [
                    {
                        id: 0, title: "Meridians/Accu Puncture", randomValue: '', frequency: '335.73',
                    },
                    {
                        id: 1, title: "Spiritual Protection", randomValue: '', frequency: '495.73',
                    },
                    {
                        id: 2, title: "Sacred Geometry", randomValue: '', frequency: '425.73',
                    }
                ];
                $rootScope.system_overview.mind = [
                    {
                        id: 0, title: "Emotional", uri: "emotional", topClassActive: "", randomValue: '',frequency:'325.73',
                        emotional: [
                        ]
                    },
                    {
                        id: 1, title: "Brain EEG", uri: "brainEeg", topClassActive: "", randomValue: '',frequency:'125.73',
                        brainEeg: [
                        ]
                    },
                    {
                        id: 2, title: "Batch Flower Essences", uri: "batchFlowerEssences", topClassActive: "", randomValue: '',
                        frequency:'355.73',
                        batchFlowerEssences: [
                        ]

                    },
                    {
                        id: 3, title: "Neurotransmitters", uri: "neurotransmitters", topClassActive: "", randomValue: '',
                        frequency:'455.73',
                        neurotransmitters: [
                        ]
                    },
                    {
                        id: 4, title: "Brain Anatomy", uri: "brainAnatomy", topClassActive: "", randomValue: '',
                        frequency:'455.73',
                        brainAnatomy: [
                        ]
                    },
                    {
                        id: 5, title: "Past Tramaus Affecting", uri: "pastTraumas", topClassActive: "", randomValue: '',
                        frequency:'455.73',
                        pastTraumas: [
                        ]
                    }
                ];
                $rootScope.system_overview.mind.brainEeg=[
                    {
                        id: 0, title: "Agrimony", randomValue: '',frequency:'335.73',
                    }
                ];
                $rootScope.system_overview.mind.pastTraumas=[
                    {
                        id: 0, title: "0-2 months", randomValue: '',frequency:'335.73',
                    },
                    {
                        id: 1, title: "3-6 months", randomValue: '',frequency:'335.73',
                    },
                    {
                        id: 2, title: "7-9 months", randomValue: '',frequency:'335.73',
                    }
                ];
                
                $rootScope.system_overview.mind.batchFlowerEssences = [
                    {
                        id: 0, title: "Agrimony", randomValue: '',frequency:'335.73',
                    },
                    {
                        id: 1, title: "Aspen", randomValue: '',frequency:'495.73',
                    },
                    {
                        id: 2, title: "Beech", randomValue: '',frequency:'425.73',
                    },
                    {
                        id: 3, title: "Centaury", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 4, title: "Cerato", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 5, title: "Chicory", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 6, title: "Clematis", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 7, title: "Crab Apple", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 8, title: "Elm", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 9, title: "Gentian", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 10, title: "Gorse", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 11, title: "Heather", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 12, title: "ch flowers", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 13, title: "Holly", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 14, title: "Hunnysuckle", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 16, title: "Homebeam", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 17, title: "Impatiens", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 18, title: "Larch", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 19, title: "Mimulus", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 20, title: "Mustard", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 21, title: "Oak", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 22, title: "Olive", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 23, title: "Pine", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 24, title: "Red Chestnut", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 25, title: "Rock Rose", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 26, title: "Rock Water", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 27, title: "Scleranthus", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 28, title: "Star of Bethlehem", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 29, title: "Sweet Chestnut", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 30, title: "Vervian", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 31, title: "Vine", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 32, title: "Walnet", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 33, title: "Water Violet", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 34, title: "White Chestnut", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 35, title: "Wild Oat", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 36, title: "Wild Rose", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 37, title: "Willlow", randomValue: '',frequency:'455.73',
                    }
                ];
                $rootScope.system_overview.mind.brainAnatomy = [
                    {
                        id: 0, title: "Corex", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 1, title: "Brain Stem", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 2, title: "Basal Ganglia", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 3, title: "Cerebellum", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 4, title: "Frontal Lobes", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 5, title: " Parietal Lobes", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 6, title: "Temporal Lobes", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 7, title: "Occipital Lobes", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 8, title: "Cranium", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 9, title: "Spinal Cord", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 10, title: "Dura", randomValue: '',frequency:'455.73',
                    }
                ];
                $rootScope.system_overview.mind.emotional = [
                    {
                        id: 0, title: "Affection", randomValue: '',frequency:'445.73',
                    },
                    {
                        id: 1, title: "Anger", randomValue: '',frequency:'415.73',
                    },
                    {
                        id: 2, title: "Angst", randomValue: '',frequency:'355.73',
                    },
                    {
                        id: 3, title: "Annoyance", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 4, title: "Anxiety", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 5, title: "Apathy", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 6, title: "Awe", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 7, title: "Borebom", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 8, title: "Confusion", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 9, title: "Contempt", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 10, title: "Curiosity", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 11, title: "Depression", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 12, title: "Desire", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 13, title: "Despair", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 14, title: "Disappointment", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 15, title: "Disgust", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 16, title: "Ecstasy", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 17, title: "Embarrassement", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 18, title: "Enpathy", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 19, title: "Envy", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 20, title: "Euphoria", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 21, title: "Fear", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 22, title: "Fear", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 23, title: "Frustration", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 24, title: "Gratitude", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 25, title: "Grief", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 26, title: "Guilt", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 27, title: "Happiness", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 28, title: "Hatred", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 29, title: "Hope", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 30, title: "Horror", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 31, title: "Hostility", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 32, title: "Hysteric", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 33, title: "Interest", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 34, title: "Jealousy", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 35, title: "Loathing", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 36, title: "Loneliness", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 37, title: "Love", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 38, title: "Lust", randomValue: '',frequency:'455.73',
                    }
                    ,
                    {
                        id: 39, title: "Misery", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 40, title: "Pity", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 41, title: "Pride", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 42, title: "Rage", randomValue: '',frequency:'455.73',
                    }
                    ,
                    {
                        id: 43, title: "Regret", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 44, title: "Remorse", randomValue: '',frequency:'455.73',
                    },
                    {
                        id: 45, title: "Sad", randomValue: '',frequency:'455.73',
                    }
                ];
                $rootScope.system_overview.mind.neurotransmitters = [
                    {
                        id: 0, title: "Serotonin", randomValue: '',frequency:'452.73',
                    },
                    {
                        id: 1, title: "Gaba", randomValue: '',frequency:'235.73',
                    },
                    {
                        id: 2, title: "Dopamine", randomValue: '',frequency:'343.73',
                    },
                    {
                        id: 3, title: "Norepinephr", randomValue: '',frequency:'454.73',
                    },
                    {
                        id: 4, title: "Epinephrine", randomValue: '',frequency:'546.73',
                    }
                ];
                $rootScope.system_overview.body = [
                    {
                        id: 0, title: "Organs", uri: "organs", randomValue: '', organs: [],frequency:'546.73',
                    },
                    {
                        id: 1, title: "Vitamins", uri: "vitamins", randomValue: '', vitamins: [],frequency:'546.73',
                    },
                    {
                        id: 2, title: "Essential Oils", uri: "essentialOils", randomValue: '', essentialOils: [],frequency:'546.73',
                    },
                    {
                        id: 3, title: "Herbs", uri: "herbs", randomValue: '', herbs: [],frequency:'546.73',
                    },
                    {
                        id: 4, title: "Minerals", uri: "minerals", randomValue: '', minerals: [],frequency:'546.73',
                    },
                    {
                        id: 5, title: "Amino Acids", uri: "aminoAcids", randomValue: '', aminoAcids: [],frequency:'546.73',
                    },
                    {
                        id: 6, title: "Glands", uri: "glands", randomValue: '', glands: [],frequency:'546.73',
                    },
                    {
                        id: 7, title: "Meridians", uri: "merdians", randomValue: '', merdians: [],frequency:'546.73',
                    },
                    {
                        id: 8, title: "Today's stress", uri: "todayStress", randomValue: '', todayStress: [],frequency:'546.73',
                    },
                    {
                        id: 9, title: "Food Senstivities", uri: "foodSenstivities", randomValue: '', foodSenstivities: [],frequency:'546.73',
                    },
                    {
                        id: 10, title: "Chemical Senstivities", uri: "chemicalSenstivitiy", randomValue: '', chemicalSenstivitiy: [],frequency:'546.73',
                    },
                    {
                        id: 11, title: "Eletrical Senstivities", uri: "electricalSenstivity", randomValue: '', electricalSenstivity: [],frequency:'546.73',
                    },
                    {
                        id: 12, title: "Body Systems", uri: "bodySystems", randomValue: '', bodySystems: [],frequency:'546.73',
                    },
                    {
                        id: 13, title: "Digestion", uri: "digestion", randomValue: '', digestion: [],frequency:'546.73',
                    },
                    {
                        id: 14, title: "Spinal Energy", uri: "spinalEnergy", randomValue: '', spinalEnergy: [],frequency:'546.73',
                    },
                    {
                        id: 15, title: "Current infections", uri: "currentInfections", randomValue: '', currentInfections: [],frequency:'546.73',
                    },
                    {
                        id: 16, title: "Hormones", uri: "hormones", randomValue: '', hormones: [],frequency:'546.73',
                    }
                ];
                 
                      $rootScope.system_overview.body.essentialOils=[
                           {
                        id: 0, title: "Acid", randomValue: '', frequency: '445.73',
                       }
                      ];
                  	$rootScope.system_overview.body.currentInfections=[
                            {
                        id: 0, title: "Acid", randomValue: '', frequency: '445.73',
                       } 
                        ];
                        $rootScope.system_overview.body.hormones=[
                            {
                        id: 0, title: "Acid", randomValue: '', frequency: '445.73',
                       } 
                        ];
                      $rootScope.system_overview.body.todayStress=[
                           {
                        id: 0, title: "Acid", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Adrenal", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Bacteria", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Blood Sugar", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Brain", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Cardiovascular", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Conception Vessel", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Connective Tissue", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Degeneration", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 9, title: "Emotional", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 10, title: "Environmental", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 11, title: "Fungus", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 12, title: "Heavy Metal Toxicity", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 13, title: "Hormonal", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 14, title: "Immune", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 15, title: "Infection", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 16, title: "Inflammation Kidneys", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 17, title: "Liver", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 18, title: "Lymphatic", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 19, title: "Nutritional", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 20, title: "Pathogens", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 21, title: "Sensitivities", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 22, title: "Virus", randomValue: '', frequency: '455.73',
                    }
                      ];
                        $rootScope.system_overview.body.foodSenstivities=[
                            {
                        id: 0, title: "Animal Fat", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Animal Hair", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Barley", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Cat", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Cheese", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Chemical", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Chocolate", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Coffee", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Corn", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 9, title: "Dairy", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 10, title: "Dander", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 11, title: "Dog", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 12, title: "Dust", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 13, title: "Egg", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 14, title: "Fish", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 15, title: "Garlic", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 16, title: "Gluten", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 17, title: "Grass", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 18, title: "Green Beans", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 19, title: "Latex", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 20, title: "Milk", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 21, title: "Mites", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 22, title: "Mold", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 23, title: "Nuts General", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 24, title: "Oats", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 25, title: "Peanuts", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 26, title: "Penicilin", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 27, title: "Perfumes", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 28, title: "Pollen", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 29, title: "Rye", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 30, title: "Salt", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 31, title: "Shellfish", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 32, title: "Soy", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 33, title: "Sugar", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 34, title: "Sulphites", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 35, title: "Tomatoes", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 36, title: "Vegetable Oil", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 37, title: "Wheat", randomValue: '', frequency: '455.73',
                    }
                        ];
                $rootScope.system_overview.body.chemicalSenstivitiy=[
                     {
                        id: 0, title: "Perfumes", frequency: '455.73',
                    },
                    {
                        id: 1, title: "Fragrances", frequency: '455.73',
                    },
                    {
                        id: 2, title: "Air Pollution", frequency: '455.73',
                    },
                    {
                        id: 3, title: "Cigarette Smoke", frequency: '455.73',
                    },
                    {
                        id: 4, title: "Solvents", frequency: '455.73',
                    },
                    {
                        id: 5, title: "VOC's", frequency: '455.73',
                    },
                    {
                        id: 6, title: "Paint Fumes", frequency: '455.73',
                    },
                    {
                        id: 7, title: "Formaldehyde", frequency: '455.73',
                    },
                    {
                        id: 8, title: "Pesticides", frequency: '455.73',
                    },
                    {
                        id: 9, title: "Herbicides", frequency: '455.73',
                    },
                    {
                        id: 10, title: "Organophosphates", frequency: '455.73',
                    },
                    {
                        id: 11, title: "Gasoline", frequency: '455.73',
                    }
                ]
                $rootScope.system_overview.body.bodySystems = [
                    {
                        id: 0, title: "Circulatory", frequency: '455.73',
                    },
                    {
                        id: 1, title: "Digestives", frequency: '455.73',
                    },
                    {
                        id: 2, title: "Endocrine", frequency: '455.73',
                    },
                    {
                        id: 3, title: "Immune", frequency: '455.73',
                    },
                    {
                        id: 4, title: "Integumentary", frequency: '455.73',
                    },
                    {
                        id: 5, title: "Lymphatic", frequency: '455.73',
                    },
                    {
                        id: 6, title: "Muscular", frequency: '455.73',
                    },
                    {
                        id: 7, title: "Nervous", frequency: '455.73',
                    },
                    {
                        id: 8, title: "Reproductive", frequency: '455.73',
                    },
                    {
                        id: 9, title: "Respiratory", frequency: '455.73',
                    },
                    {
                        id: 10, title: "Skeletal", frequency: '455.73',
                    },
                    {
                        id: 11, title: "Prostate", frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.body.minerals = [
                    {
                        id: 0, title: "Copper", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Chromium", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Iodine", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Iron", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Molybdenum", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Manganese", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Selenium", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Zinc", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Magnesium", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 9, title: "Potassium", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 10, title: "Sulfate", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 11, title: "Boron", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 12, title: "Calcium", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 13, title: "Caprylic Acid", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 14, title: "Silica", randomValue: '', frequency: '455.73',
                    }
                ];

                $rootScope.system_overview.body.aminoAcids = [
                    {
                        id: 0, title: "Alanine", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Arginine", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Asparagine", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Aspartic acid", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Cysteine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Glutamic acid", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Glutamine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Glycine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Histidine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 9, title: "Isoleucine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 10, title: "Leucine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 11, title: "Lysine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 12, title: "Methionine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 13, title: "Phenylalanine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 14, title: "Proline", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 15, title: "Serine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 16, title: "Threonine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 17, title: "Tryptophan", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 18, title: "Tyrosine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 19, title: "Valine", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.body.glands = [
                    {
                        id: 0, title: "Adrenal", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Hypothalamus", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Pineal", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Pituitary", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Thyroid", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Thymus", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Pancreas", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Testes", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Ovaries", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.body.merdians = [
                    {
                        id: 0, title: "Bladder", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Conception Vessel", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Gall Bladder", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Governing Meridian", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "heart", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Kidney", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Large Intestine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Liver", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Lung", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 9, title: "Pericardium", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 10, title: "Small Intestine", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 11, title: "Spleen", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 12, title: "Stomach", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 13, title: "Triple Heater", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.body.organs = [
                    {
                        id: 0, title: "Brain", frequency: '455.73', randomValue: ''
                    },
                    {
                        id: 1, title: "Eyes", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 2, title: "Skin", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 3, title: "Thyroid", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 4, title: "Lungs", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 5, title: "Heart", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 6, title: "Liver", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 7, title: "Pancreas", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 8, title: "Stomach", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 9, title: "Spleen", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 10, title: "Kidneys", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 11, title: "Prostate", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 12, title: "Bladder", frequency: '455.73',randomValue: ''
                    }
                ];		
                $rootScope.system_overview.body.currentInfections=[
                     {
                        id: 0, title: "Virus", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 1, title: "Parasites", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 2, title: "Bacteria", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 3, title: "Biofilm", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 4, title: "Lyme", frequency: '455.73',randomValue: ''
                    }
                ];
                $rootScope.system_overview.body.vitamins=[
                    {
                        id: 0, title: "Vitamin A", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 1, title: "Vitamin B1", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 2, title: "Vitamin B2", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 3, title: "Vitamin B3", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 4, title: "Vitamin B4", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 5, title: "Vitamin B5", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 6, title: "Vitamin B6", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 7, title: "Vitamin B12", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 8, title: "Biotin", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 9, title: "Vitamin C", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 10, title: "Vitamin D", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 11, title: "Vitamin E", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 12, title: "Folic Acid", frequency: '455.73',randomValue: ''
                    },
                    {
                        id: 13, title: "Vitamin K", frequency: '455.73',randomValue: ''
                    }
                ];
                $rootScope.system_overview.body.spinalEnergy=[
                     {
                        id: 0, title: "C1", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 1, title: "C2", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 2, title: "C3", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 3, title: "C4", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 4, title: "C5", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 5, title: "C6", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 6, title: "C7", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 7, title: "Th1", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 8, title: "Th2", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 9, title: "Th3", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 10, title: "Th4", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 11, title: "Th5", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 12, title: "Th6", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 13, title: "Th7", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 14, title: "Th8", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 15, title: "Th9", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 16, title: "Th10", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 17, title: "Th11", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 18, title: "Th12", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 19, title: "L1", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 20, title: "L2", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 21, title: "L3", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 22, title: "L4", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 23, title: "L5", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 24, title: "Sacral", frequency: '455.73',randomValue: ''
                    }
                ];
                $rootScope.system_overview.body.bodySystems=[
                    {
                        id: 0, title: "Circulatory", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 1, title: "Digestive", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 2, title: "Endocrine", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 3, title: "Immune", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 4, title: "Integumentary", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 5, title: "Lymphatic", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 6, title: "Muscular", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 7, title: "Nervous", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 8, title: "Reproductive", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 9, title: "Respiratory", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 10, title: "Skeletal", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 11, title: "Urinary", frequency: '455.73',randomValue: ''
                    }
                ];
                $rootScope.system_overview.body.digestion=[
                     {
                        id: 0, title: "Liver", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 1, title: "Pancreas", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 2, title: "Gall Bladder", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 3, title: "Mouth", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 4, title: "Esophagus", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 5, title: "Stomach", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 6, title: "Small Intestine", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 7, title: "Large Intestine", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 8, title: "Flora", frequency: '455.73',randomValue: ''
                    },
                     {
                        id: 9, title: "Enzymes", frequency: '455.73',randomValue: ''
                    }
                ];					
                   $rootScope.system_overview.body.electricalSenstivity=[
                        {
                        id: 0, title: "computer monitors", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "electrical appliances", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Wifi", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "UHF", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Cellular Frequencies", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Radio Waves", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Fluorescent Lights", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Metallic Implants", randomValue: '', frequency: '455.73',
                    }
                   ];

                $rootScope.system_overview.body.herbs = [
                    {
                        id: 0, title: "Alfalfa", randomValue: '', frequency: '445.73',
                    },
                    {
                        id: 1, title: "Aloe", randomValue: '', frequency: '415.73',
                    },
                    {
                        id: 2, title: "Ginseng", randomValue: '', frequency: '355.73',
                    },
                    {
                        id: 3, title: "Amla", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Angelica", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Anise", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Arnica", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Ashwagandha", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Astragalus", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 9, title: "Bacopa", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 10, title: "Bearberry", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 11, title: "Bee Balm", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 12, title: "Bee Pollen", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 13, title: "Billberry", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 14, title: "Black Cherry", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 15, title: "Black Cohosh", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 16, title: "Boneset", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 17, title: "Borage", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 18, title: "Boswellia", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 19, title: "Buchu", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 20, title: "Burdock", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 21, title: "Butterbur", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 22, title: "Calendula", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 23, title: "Cascara Sagrada", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 24, title: "Catnip", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 25, title: "Cat�s Claw", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 26, title: "Cayenne", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 27, title: "Chamomile", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 28, title: "Chaparral", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 29, title: "Chaste Tree", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 30, title: "Chicory", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 31, title: "Cinnamon", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 32, title: "Club Moss", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 33, title: "Comfrey", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 34, title: "Cordyceps", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 35, title: "Dandelion", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 36, title: "Dong Quai", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 37, title: "Echinacea", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 38, title: "Fo-Ti", randomValue: '', frequency: '455.73',
                    }
                    ,
                    {
                        id: 39, title: "Ginkgo Biloba", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 40, title: "Gotu Kola", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 41, title: "Gynostemma", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 42, title: "Holy Basil", randomValue: '', frequency: '455.73',
                    }
                    ,
                    {
                        id: 43, title: "Kava Kava", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 44, title: "Korean Ginseng", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 45, title: "Lemon Grass", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 46, title: "Chinese Licorice Root", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 47, title: "Lion�s Mane", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 48, title: "Lycium Fruit", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 49, title: "Maca", randomValue: '', frequency: '455.73',
                    }
                    ,
                    {
                        id: 50, title: "Milk Thistle", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 51, title: "Maitake", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 52, title: "Rhodiola", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 53, title: "Saw Palmetto", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 54, title: "Schizandra", randomValue: '', frequency: '455.73',
                    }
                    ,
                    {
                        id: 55, title: "Shilajit", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 56, title: "Siberian Ginseng", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 57, title: "Skullcap", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 58, title: "St. John�s Wort", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 59, title: "Suma", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 60, title: "Turmeric", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 61, title: "Valerian Root", randomValue: '', frequency: '455.73',
                    }
                ]
                $rootScope.system_overview.energy = [
                    {
                        id: 0, title: "Soifeggio Tones", uri: "soifeggioTones", randomValue: '', soifeggioTones: []
                    },
                    {
                        id: 1, title: "Chakras", uri: "chakras", randomValue: '', chakras: []
                    },
                    {
                        id: 2, title: "Auras", uri: "auras", randomValue: '', auras: []
                    },
                    {
                        id: 3, title: "nogier", uri: "nogier", randomValue: '', nogier: []
                    }
                ];
                $rootScope.system_overview.energy.soifeggioTones = [
                    {
                        id: 0, title: "Soifeggio Tones", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.energy.chakras = [
                    {
                        id: 0, title: "Crown Chakra", randomValue: ''
                    },
                    {
                        id: 1, title: "Throat Chakra", randomValue: ''
                    },
                    {
                        id: 2, title: "Heart Chakra", randomValue: ''
                    },
                    {
                        id: 3, title: "Solar Chakra", randomValue: ''
                    },
                    {
                        id: 4, title: "Sacral Chakra", randomValue: ''
                    },
                    {
                        id: 5, title: "Root Chakra", randomValue: ''
                    }
                ];
                $rootScope.system_overview.energy.auras = [
                    {
                        id: 0, title: "Soifeggio Tones", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.energy.nogier = [
                    {
                        id: 0, title: "Nogier A", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 1, title: "Nogier B", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 2, title: "Nogier C", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 3, title: "Nogier D", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "Nogier E", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 5, title: "Nogier F", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 6, title: "Nogier G", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 7, title: "Nogier H", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 8, title: "Nogier I", randomValue: '', frequency: '455.73',
                    },
                ];
                $rootScope.system_overview.spirituality = [
                    {
                        id: 0, title: "item1", randomValue: '', frequency: '455.73',uri:'item1',item1:[]
                    },
                    {
                        id: 1, title: "item2", randomValue: '', frequency: '455.73',uri:'item2',item2:[]
                    },
                    {
                        id: 2, title: "Sacred Geometry", randomValue: '', frequency: '455.73',uri:'sacredGeometry',sacredGeometry:[]
                    }
                ];
                $rootScope.system_overview.spirituality.item1=[
                    {
                        id: 0, title: "item1", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.spirituality.item2=[
                    {
                        id: 0, title: "item1", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.spirituality.sacredGeometry=[
                    {
                        id: 0, title: "item1", randomValue: '', frequency: '455.73',
                    }
                ];
                $rootScope.system_overview.libraries = [
                    {
                        id: 0, title: "item1", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 1, title: "item2", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 2, title: "item3", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 3, title: "item4", randomValue: '', frequency: '455.73',
                    },
                    {
                        id: 4, title: "item5", randomValue: '', frequency: '455.73',
                    }
                ];

                // Set uuid & platform work only on device
                try {
                    $rootScope.uuid = $cordovaDevice.getUUID();
                    $rootScope.platform = $cordovaDevice.getPlatform();
                } catch (e) {
                    $rootScope.uuid = '';
                    $rootScope.platform = 'browser';
                }



                // Offline userLogin Count

                $rootScope.email_filter = /^[a-zA-Z0-9._]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
                $rootScope.name= /^[a-zA-Z\s]*$/;
                $rootScope.digit= /^[0-9]+$/;
                $rootScope.item_added= /^[a-zA-Z0-9 ]*$/;


                if (window.cordova && window.cordova.plugins.Keyboard) {
                    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
                    // for form inputs)
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                    // Don't remove this line unless you know what you are doing. It stops the viewport
                    // from snapping when text inputs are focused. Ionic handles this internally for
                    // a much nicer keyboard experience.
                    cordova.plugins.Keyboard.disableScroll(true);
                }
                if (window.StatusBar) {
                    StatusBar.styleDefault();
                }

//                  // Login User Detail Store
//                if(!$localStorage.user){
//                    $localStorage.user = '';
//                }
            });
        })
        .config(function ($stateProvider, $urlRouterProvider, $httpProvider, $localStorageProvider) {
//            $httpProvider.defaults.headers.put = { 
//                'Accept' : 'application/json',
//                'Content-Type': 'application/json'
//            };

            $stateProvider
                    .state('app', {
                        url: '/app',
                        abstract: true,
                        templateUrl: 'templates/menu.html',
                        controller: 'menuCtrl'
                    })
                    .state('login', {
                        url: '/login',
                        templateUrl: 'templates/login.html',
                        controller: 'loginCtrl'

                    })
                    .state('app.voice_analysis', {
                        url: '/voice_analysis/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/voice_analysis.html',
                                controller: 'voiceAnalysisCtrl'
                            }
                        }
                    })
                    .state('app.accessories', {
                        url: '/accessories',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/accessories.html',
                                controller: 'accessoriesCtrl'
                            }
                        }
                    })
                    .state('app.supportive', {
                        url: '/supportive',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/supportive.html',
                                controller: 'supportiveCtrl'
                            }
                        }
                    })
                    .state('app.setting', {
                        url: '/setting',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/setting.html',
                                controller: 'settingCtrl'
                            }
                        }
                    })
                    .state('app.frequency', {
                        url: '/frequency',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/frequency.html',
                                controller: 'frequencyCtrl'
                            }
                        }
                    })
                    .state('app.libraries', {
                        url: '/libraries',
                        cache:false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/libraries.html',
                                controller: 'librariesCtrl'
                            }
                        }
                    })
                    .state('app.add_library', {
                        url: '/add_library',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/add_library.html',
                                controller: 'addLibraryCtrl'
                            }
                        }
                    })
                    .state('library_detail', {
                        url: '/library_detail/:id/:name',
                        templateUrl: 'templates/library_detail.html',
                        controller: 'libraryDetailCtrl'
                    })
                    .state('app.list_user', {
                        url: '/list_user/:id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/list_user.html',
                                controller: 'listUserCtrl'
                            }
                        }
                    })
                    .state('app.library_items', {
                        url: '/library_items/:library_id/:library_name',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/library_items.html',
                                controller: 'libraryItemsCtrl'
                            }
                        }
                    })
		       .state('app.heading', {
                        url: '/heading',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/heading.html',
                                controller: 'headingCtrl'
                            }
                        }
                    })
                    .state('app.recheck', {
                        url: '/recheck',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/recheck.html',
                                controller: 'recheckCtrl'
                            }
                        }
                    })
                     .state('app.cross_analysis', {
                        url: '/cross_analysis',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/cross_analysis.html',
                                controller: 'crossAnalysisCtrl'
                            }
                        }
                    })
                     .state('app.mep', {
                        url: '/mep',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/mep.html',
                                controller: 'mepCtrl'
                            }
                        }
                    })
                    .state('bio_compatibility', {
                        url: '/bio_compatibility',
                        templateUrl: 'templates/bio_compatibility.html',
                        controller: 'bioCompatibilityCtrl'
                    })
                    .state('app.image_analysis', {
                        url: '/image_analysis/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/image_analysis.html',
                                controller: 'imageAnalysisCtrl'
                            }
                        }
                    })

                    .state('app.system_overview', {
                        url: '/system_overview/:user_record_id',
                        cache:false,
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/system_overview.html',
                                controller: 'systemOverviewCtrl'
                            }
                        }
                    })

                    .state('app.system_overview_two', {
                        url: '/system_overview_two/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/system_overview_two.html',
                                controller: 'systemOverviewTwoCtrl'
                            }
                        }
                    })
                    .state('app.system_overview_three', {
                        url: '/system_overview_three/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/system_overview_three.html',
                                controller: 'systemOverviewThreeCtrl'
                            }
                        }
                    })
                    .state('app.past_record', {
                        url: '/past_record/:record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/past_record.html',
                                controller: 'pastRecordCtrl'
                            }
                        }
                    })
                    .state('forget_password', {
                        url: '/forget_password',
                        templateUrl: 'templates/forget_password.html',
                        controller: 'forgetPasswordCtrl'
                    })
                    .state('signup', {
                        url: '/signup',
                        templateUrl: 'templates/signup.html',
                        controller: 'signupCtrl'
                    })
                    .state('account_activation', {
                        url: '/account_activation',
                                templateUrl: 'templates/account_activation.html',
                                controller: 'accountActivationCtrl'
                           
                    })
                    .state('app.user_records', {
                        url: '/user_records/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/user_records.html',
                                controller: 'userRecordsCtrl'
                            }
                        }
                    })
                    .state('otp', {
                        url: '/otp',
                        templateUrl: 'templates/otp.html',
                        controller: 'otpCtrl'
                    })
                    .state('app.user_record_one', {
                        url: '/user_record_one',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/user_record_one.html',
                                controller: 'userRecordOneCtrl'
                            }
                        }
                    })
                    .state('app.user_record_two', {
                        url: '/user_record_two/:user_record_id',
                        views: {
                            'menuContent': {
                                templateUrl: 'templates/user_record_two.html',
                                controller: 'userRecordTwoCtrl'
                            }
                        }
                    });

            // If user is logged in then redirect on user record page        
            if ($localStorageProvider.$get().user == undefined) {
                $urlRouterProvider.otherwise("/login");
            } else {
                $urlRouterProvider.otherwise("/app/user_record_one");
            }
        });
